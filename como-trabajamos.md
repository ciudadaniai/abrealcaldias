---
layout: page-como-trabajamos
sub-title:
title: Cómo trabajamos
permalink: /como-trabajamos/
welcome_desktop: ../assets/images/banners/banner-como-trabajamos.png
welcome_mobile: ../assets/images/banners/banner-como-trabajamos.png
---


<section class="uk-container uk-container" id="como-trabajamos">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">Un <span>método</span> de trabajo para la apertura</h3>
        </div>

        <div class="uk-width-1">

            <div class="box animated fadeInUp delay-1s fast">
               <div class="img-box">
                   <img src="/assets/images/banners/formacion-img.png" alt="img" class="image-box" width="100%">
                </div>
                <div class="iconos">
                    <img src="/assets/images/banners/formacion-icono-2.png" alt="img" class="image-box-icono" width="100%">
                 </div>
                <div class="informacion">
                    <h4>Formación</h4>
                    <p>Ofrecemos una formación gratuita en formato virtual y entregamos herramienta para el diseño de políticas públicas participativas, innovadoras y abiertas.</p>
                    <!--<a href="#" class="btn">Proceso de formación</a> -->
                 </div>
            </div>

            <div class="box-invertido animated fadeInUp delay-1s fast">
               <div class="img-box">
                   <img src="/assets/images/banners/estrategia-img.png" alt="img" class="image-box" width="100%">
                </div>
                <div class="iconos">
                    <img src="/assets/images/banners/estrategia-icono-2.png" alt="img" class="image-box-icono" width="100%">
                 </div>
                <div class="informacion">
                    <h4>Estrategia</h4>
                    <p>Herramientas para co-construir soluciones  con foco en los territorios</p>
                  <!--  <a href="#" class="btn">Nuestra estrategía</a>-->
                 </div>
            </div>

            <div class="box animated fadeInUp delay-1s fast">
               <div class="img-box">
                   <img src="/assets/images/banners/plan-img.png" alt="img" class="image-box" width="100%">
                </div>
                <div class="iconos">
                    <img src="/assets/images/banners/plan-icono-2.png" alt="img" class="image-box-icono" width="100%">
                 </div>
                <div class="informacion">
                    <h4>Plan de acción</h4>
                    <p>Realizamos diagnósticos con los gobiernos locales y generamos planes personalizados para la promoción de políticas públicas  </p>
                    <!--<a href="#" class="btn">Nuestro plan</a>-->
                 </div>
            </div>

            <div class="box-invertido animated fadeInUp delay-1s fast">
               <div class="img-box">
                   <img src="/assets/images/banners/network-img.png" alt="img" class="image-box" width="100%">
                </div>
                <div class="iconos">
                    <img src="/assets/images/banners/network-icono-2.png" alt="img" class="image-box-icono" width="100%">
                 </div>
                <div class="informacion">
                    <h4>Network</h4>
                    <p>Impulsamos una red de innovadores municipales latinoamericanos para intercambiar prácticas y experiencias y organizamos masterclass con experts en gobierno abierto e innovación</p>
                  <!--  <a href="#" class="btn">Network</a>-->
                 </div>
            </div>


        </div>

    </div>
</section>


<section id="cta-abre">
    <div class="uk-container uk-container-small uk-padding">
         <div class="uk-width-1 uk-flex uk-flex-center uk-flex-around@s uk-flex-middle uk-flex-wrap">
             <h3 class="animated fadeInUp delay-1s fast uk-text-center uk-text-center@s uk-text-left@m"><span>Una red de innovadores</span><br>
municipales en América Latina</h3>
             <!--modal -->
             <a href="https://forms.gle/UjPjP3xW9vUDmEaTA" class="animated fadeInUp delay-1s fast btn-abre" target="_blank">Se parte</a>
         <!-- This is the modal -->
         <!--<div id="modal-example" uk-modal>
             <div class="uk-modal-dialog uk-modal-body">
                 <h2 class="uk-modal-title">Headline</h2>
                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                 <p class="uk-text-right">
                     <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                     <button class="uk-button uk-button-primary" type="button">Save</button>
                 </p>
             </div>
         </div>-->

         </div>
    </div>
</section>

<div class="bg-herramientas">
<section class="uk-container uk-container-medium" id="herramientas-cta">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1 informacion">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">Creamos herramientas</h3>
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">para los gobiernos locales</h3>
            <a href="{{ site.baseurl_root }}/nuestras-herramientas" class="btn-abre animated fadeInUp delay-1s fast uk-text-center">Ver herramientas</a>
        </div>
    </div>
</section>
</div>
