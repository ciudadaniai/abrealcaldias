---
layout: page-nuestras-herramientas
sub-title: Cómo trabajamos
title: Nuestras herramientas
permalink: /nuestras-herramientas/
welcome_desktop: ../assets/images/banners/banner-herramientas.png
welcome_mobile: ../assets/images/banners/banner-herramientas.png
---

<div class="fondo-herramientas">
<section class="uk-container uk-container-medium uk-margin" id="herramientas">
    <div class="uk-flex uk-flex-center" uk-grid>
        <div class="uk-width-1">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">Creamos herramientas para el fortalecimiento de gobiernos locales</h3>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m herramienta-box">
            <img src="/assets/images/banners/herramienta-1-img.png" alt="img" class="img-herramienta" width="100%">
            <div class="descripcion">
               <h3 class="animated fadeInUp delay-1s fast uk-text-center">Manual Abre Alcaldías</h3>
               <a href="https://gitlab.com/pedregalux/images2021/-/raw/master/abrealcaldias-book.pdf" target="_blank" class="btn">Descargar</a>
            </div>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m herramienta-box">
            <img src="/assets/images/banners/herramienta-2-img.png" alt="img" class="img-box" width="100%">
            <div class="descripcion">
               <h3 class="animated fadeInUp delay-1s fast uk-text-center">Iniciativas de participación en gobierno local</h3>
               <a href="https://docs.google.com/document/d/1OBi0Ul-Jb-cOk9yxPurDmvZPh6DfNfe_5NBkwd7KRcw/edit" target="_blank" class="btn">Descargar</a>
            </div>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m herramienta-box">
            <img src="/assets/images/banners/herramienta-3-img.png" alt="img" class="img-box" width="100%">
            <div class="descripcion">
               <h3 class="animated fadeInUp delay-1s fast uk-text-center">KIT para el trabajo colaborativo remoto</h3>
               <a href="https://docs.google.com/document/d/1qRn6mwYgprjHcV3UkCwTDxPCX1To4m7Ag6d_Jf7KtwA/edit?usp=drivesdk" target="_blank" class="btn">Descargar</a>
            </div>
        </div>

        <div class="uk-width-1 mensaje">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">La participación en Abre Alcaldías <span>no tiene costo</span></h3>
        </div>

    </div>
</section>
</div>

<div class="img-grupo">
    <img src="/assets/images/banners/fondo-personas-2.png" alt="img grupo abre alcaldías" class="img" width="100%">
</div>

<section id="cta-abre" class="bg-celeste">
    <div class="uk-container uk-container-small uk-padding">
         <div class="uk-width-1 uk-flex uk-flex-center uk-flex-around@s uk-flex-middle uk-flex-wrap">
             <h3 class="animated fadeInUp delay-1s fast uk-text-center uk-text-center@s uk-text-left@m"><span>Una red de innovadores</span><br>
municipales en América Latina</h3>
             <!--modal -->
             <a href="https://forms.gle/UjPjP3xW9vUDmEaTA" class="animated fadeInUp delay-1s fast btn-abre" target="_blank">Se parte</a>
         <!-- This is the modal -->
         <!--<div id="modal-example" uk-modal>
             <div class="uk-modal-dialog uk-modal-body">
                 <h2 class="uk-modal-title">Headline</h2>
                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                 <p class="uk-text-right">
                     <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                     <button class="uk-button uk-button-primary" type="button">Save</button>
                 </p>
             </div>
         </div>-->

         </div>
    </div>
</section>
