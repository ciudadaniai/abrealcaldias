---
layout: page
title: El proyecto
permalink: /about/
welcome_desktop: ../assets/images/about-desktop.jpg
welcome_mobile: ../assets/images/about-mobile.jpg
---

<div class="uk-section section-no-color animated fadeIn delay-1s fast">
  <div class="uk-container uk-width-3-4@m" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
      <div class="uk-width-3-4@m">
      <h2>{% t about.about.title %}</h2>
      <p>{% t about.about.description_1 %}</p>
      <p>{% t about.about.description_2 %}</p>
      </div>
      <div class="uk-width-1-4@m">
      </div>
    </div>
  </div>
</div>

<div class="uk-section section-color-1st animated fadeIn">
  <div class="uk-container uk-width-3-4@m" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1 uk-text-center ">
        <h2>{% t about.methodology.title %}</h2>
        <p class="font-lead">{% t about.methodology.lead %}</p>
      </div>
      <div class="uk-width-1-1 uk-flex uk-flex-middle" uk-grid>
        <div class="uk-width-1-4@m">
          <p class="font-600 font-mono font-color-2nd-0" style="font-size: 50px; margin: -40px 0 -80px;">{% t about.methodology.item_1.number %}</p >
          <h4 class="no-project-color">{% t about.methodology.item_1.title %}</h4>
        </div>
        <div class="uk-width-3-4@m">
          <p>{% t about.methodology.item_1.description %}</p>
        </div>
      </div>
      <div class="uk-width-1-1 uk-flex uk-flex-middle" uk-grid>
        <div class="uk-width-1-4@m">
          <p class="font-600 font-mono font-color-2nd-0" style="font-size: 50px; margin: -40px 0 -80px;">{% t about.methodology.item_2.number %}</p>
          <h4 class="no-project-color">{% t about.methodology.item_2.title %}</h4>
        </div>
        <div class="uk-width-3-4@m">
          <p>{% t about.methodology.item_2.description %}</p>
        </div>
      </div>
      <div class="uk-width-1-1 uk-flex uk-flex-middle" uk-grid>
        <div class="uk-width-1-4@m">
          <p class="font-600 font-mono font-color-2nd-0" style="font-size: 50px; margin: -40px 0 -80px;">{% t about.methodology.item_3.number %}</p>
          <h4 class="no-project-color">{% t about.methodology.item_3.title %}</h4>
        </div>
        <div class="uk-width-3-4@m">
          <p>{% t about.methodology.item_3.description %}</p>
        </div>
      </div>
      <div class="uk-width-1-1 uk-flex uk-flex-middle" uk-grid>
        <div class="uk-width-1-4@m">
          <p class="font-600 font-mono font-color-2nd-0" style="font-size: 50px; margin: -40px 0 -80px;">{% t about.methodology.item_4.number %}</p>
          <h4 class="no-project-color">{% t about.methodology.item_4.title %}</h4>
        </div>
        <div class="uk-width-3-4@m">
          <p>{% t about.methodology.item_4.description %}</p>
        </div>
      </div>
      <div class="uk-width-1-1 uk-flex uk-flex-middle" uk-grid>
        <div class="uk-width-1-4@m">
          <p class="font-600 font-mono font-color-2nd-0" style="font-size: 50px; margin: -40px 0 -80px;">{% t about.methodology.item_5.number %}</p>
          <h4 class="no-project-color">{% t about.methodology.item_5.title %}</h4>
        </div>
        <div class="uk-width-3-4@m">
          <p>{% t about.methodology.item_5.description %}</p>
        </div>
      </div>
      <div class="uk-width-1-1 uk-flex uk-flex-middle" uk-grid>
        <div class="uk-width-1-4@m">
          <p class="font-600 font-mono font-color-2nd-0" style="font-size: 50px; margin: -40px 0 -80px;">{% t about.methodology.item_6.number %}</p>
          <h4 class="no-project-color">{% t about.methodology.item_6.title %}</h4>
        </div>
        <div class="uk-width-3-4@m">
          <p>{% t about.methodology.item_6.description %}</p>
        </div>
      </div>
      <div class="uk-width-1-1 uk-flex uk-flex-middle" uk-grid>
        <div class="uk-width-1-4@m">
          <p class="font-600 font-mono font-color-2nd-0" style="font-size: 50px; margin: -40px 0 -80px;">{% t about.methodology.item_7.number %}</p>
          <h4 class="no-project-color">{% t about.methodology.item_7.title %}</h4>
        </div>
        <div class="uk-width-3-4@m">
          <p>{% t about.methodology.item_7.description %}</p>
        </div>
      </div>                                  
    </div>
  </div>
</div>
