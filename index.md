---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---
<script src="https://my.hellobar.com/d757debc0980262893eeae702599632565cb755f.js" type="text/javascript" charset="utf-8" async="async"></script>



<section class="uk-container uk-container-small uk-margin" id="que-es">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <p class="que-es-descripcion animated fadeInUp delay-1s fast uk-text-center">Abre Alcaldías es un proyecto de fortalecimiento de capacidades para la gestión pública enfocado en la promoción de la innovación, mecanismos de participación ciudadana y representación diversa en el proceso de toma de decisiones.</p>
        </div>
    </div>
</section>



<section class="uk-container uk-container-medium" id="ofrecemos">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">Nuestro Método</h3>
        </div>

        <div class="uk-width-1@m uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <img src="/assets/images/banners/formacion-icono.png" alt="img icono" class="img-icono animated fadeIn delay-1s fast" width="100%">
            <h4 class="animated fadeInUp delay-1s fast">Formación</h4>
            <p class="animated fadeInUp delay-1s fast uk-text-center">Contenidos de apertura y diseño colaborativo para servidores públicos </p>
        </div>

        <div class="uk-width-1@s uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
          <img src="/assets/images/banners/estrategia-icono.png" alt="img icono" class="img-icono animated fadeIn delay-1s fast" width="100%">
          <h4 class="animated fadeInUp delay-1s fast">Estrategia</h4>
          <p class="animated fadeInUp delay-1s fast uk-text-center">Herramientas para co-construir soluciones  con foco en los territorios</p>
        </div>

        <div class="uk-width-1@s uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <img src="/assets/images/banners/accion-icono.png" alt="img icono" class="img-icono animated fadeIn delay-1s fast" width="100%">
            <h4 class="animated fadeInUp delay-1s fast">Plan de acción</h4>
            <p class="animated fadeInUp delay-1s fast uk-text-center">Implementación y acompañamiento en acciones colaborativas en gobiernos locales</p>
        </div>

        <div class="uk-width-1@s uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <img src="/assets/images/banners/network-icono.png" alt="img icono" class="img-icono animated fadeIn delay-1s fast" width="100%">
            <h4 class="animated fadeInUp delay-1s fast">Network</h4>
            <p class="animated fadeInUp delay-1s fast uk-text-center">Red latinoamericana de municipios y organizaciones para aprender y compartir buenas prácticas</p>
        </div>

    </div>
</section>


<section class="uk-container uk-container-medium uk-margin" id="desafios">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">Nuestro impacto</h3>
            <!--<h3 class="animated fadeInUp delay-1s fast uk-text-center">Trabajamos con gobiernos locales por gobernanzas más <span>representativas, inclusivas y sostenibles</span> en América Latina</h3>-->
        </div>

        <div class="uk-width-1@s uk-width-1-3@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center numero">6</h3>
            <p class="animated fadeInUp delay-1s fast uk-text-center">Países</p>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center numero">+25</h3>
            <p class="animated fadeInUp delay-1s fast uk-text-center">Proyectos</p>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center numero">+55</h3>
            <p class="animated fadeInUp delay-1s fast uk-text-center">Municipios</p>
        </div>
    </div>
</section>



<section id="cta-abre">
    <div class="uk-container uk-container-small uk-padding">
         <div class="uk-width-1 uk-flex uk-flex-center uk-flex-around@s uk-flex-middle uk-flex-wrap">
             <h3 class="animated fadeInUp delay-1s fast uk-text-center uk-text-center@s uk-text-left@m"><span>Únete a nuestra</span> Red de innovadores municipales<br></h3>
             <!--modal -->
             <a href="https://forms.gle/UjPjP3xW9vUDmEaTA" class="animated fadeInUp delay-1s fast btn-abre" target="_blank">Sé parte</a>
         </div>
    </div>
</section>


<section id="municipios-paises">
    <div class="uk-container uk-container-medium row">
         <div class="content-map uk-margin-top">
             <h3 class="animated fadeInUp delay-1s fast title uk-text-bold uk-margin-remove-bottom">Historias que transforman</h3>

             <div class="slide-map uk-margin-top">
             <div class="uk-slider-container-offset" uk-slider>
                 <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                     <ul class="uk-slider-items uk-child-width-1-2@s uk-grid">


                     <li>
                         <div class="uk-card uk-card-default uk-padding-small uk-padding-remove-left uk-padding-remove-right">
                            <div class="uk-card-media-top uk-margin-left uk-margin-right">
                               <p class="animated fadeInUp delay-1s fast pais uk-text-center">Guatemala</p>
                               <h4 class="animated fadeInUp delay-1s fast uk-margin-remove-bottom nombre-municipio uk-text-center">San Antonio Sacatepéquez</h4>
                             </div>
                             <div class="uk-card-body uk-margin-top uk-margin-bottom">
                                 <div class="img-card animated fadeInUp delay-1s fast">
                                     <img src="/assets/images/banners/portada-guatemala.png" alt="img municipio" class="img-municipio" width="100%">
                                     <div class="filtrer-gradient"></div>
                                 </div>
                             </div>
                             <div class="uk-card-media-bottom uk-margin-left uk-margin-right">
                                 <h3 class="animated fadeInUp delay-1s fast uk-text-bold uk-margin-remove-top uk-text-center titulo-experiencia">Ampliando el funcionamiento de Secretarías a la participación ciudadana</h3>
                                 <a href="#modal-example-experiencia-5" uk-toggle class="btn animated fadeInUp delay-1s fast">Conoce la historia</a>
                             <!-- This is the modal -->
                             <div id="modal-example-experiencia-5" uk-modal>
                                 <div class="uk-modal-dialog uk-modal-body modal-experiencias">
                                     <button class="uk-modal-close-default" type="button" uk-close></button>
                                     <p class="pais">Guatemala</p>
                                     <h4 class="nombre-municipio">San Antonio Sacatepéquez</h4>
                                     <h2 class="uk-modal-title">Ampliando el funcionamiento de Secretarías a la participación ciudadana</h2>
                                     <p>En un ánimo de generar información sobre transparencia y acceso a la información de manera participativa, se realizaron una serie de talleres dirigidos a la juventud, líderes y lideresas coordinado con el Alcalde y Concejo Municipal para que, a través de ellos, la comunidad se conectara con la Unidad de Acceso a la Información Pública y la Oficina Municipal de Juventud.</p>
                                     <p>A partir de esto, se generó una propuesta de funcionamiento de las secretarías de juventudes para contemplar, de manera sostenible, la participación ciudadana y la implementación de procesos de difusión y sensibilización como los implementados en el marco del proyecto. Este plan se presentó ante el Consejo Municipal de Desarrollo (COMUDE), el gobierno municipal y a organizaciones de la sociedad civil.</p>

                                     <div class="video">

                                     </div>

                                     <div class="carrousel-fotos">

                                     <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow>
                                         <ul class="uk-slideshow-items">
                                             <li>
                                                 <img src="/assets/images/banners/guatemala-img-experiencia-1.png" alt="img municipio" class="img-municipio" width="100%" uk-cover>
                                             </li>
                                             <li>
                                                 <img src="/assets/images/banners/guatemala-img-experiencia-2.png" alt="img municipio" class="img-municipio" width="100%" uk-cover>
                                             </li>
                                         </ul>

                                         <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"> <</a>
                                         <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next">  > </a>
                                     </div>

                                     </div>



                                 </div>
                             </div>

                              </div>
                         </div>
                     </li>



<!-- Ecuador -->

                  <!--   <li>
                         <div class="uk-card uk-card-default uk-padding-small uk-padding-remove-left uk-padding-remove-right">
                            <div class="uk-card-media-top uk-margin-left uk-margin-right">
                               <p class="animated fadeInUp delay-1s fast pais uk-text-center">Ecuador</p>
                               <h4 class="animated fadeInUp delay-1s fast uk-margin-remove-bottom nombre-municipio uk-text-center">Portoviejo</h4>
                             </div>
                             <div class="uk-card-body uk-margin-top uk-margin-bottom">
                                 <div class="img-card animated fadeInUp delay-1s fast">
                                     <img src="/assets/images/banners/portada-guatemala.png" alt="img municipio" class="img-municipio" width="100%">
                                     <div class="filtrer-gradient"></div>
                                 </div>
                             </div>
                             <div class="uk-card-media-bottom uk-margin-left uk-margin-right">
                                 <h3 class="animated fadeInUp delay-1s fast uk-text-bold uk-margin-remove-top uk-text-center titulo-experiencia">Construyendo participación ciudadana en nuestros territorios</h3>
                                 <a href="#modal-example-experiencia-5" uk-toggle class="btn animated fadeInUp delay-1s fast">Conoce la historia</a>

                             <div id="modal-example-experiencia-5" uk-modal>
                                 <div class="uk-modal-dialog uk-modal-body modal-experiencias">
                                     <button class="uk-modal-close-default" type="button" uk-close></button>
                                     <p class="pais">Ecuador</p>
                                     <h4 class="nombre-municipio">Portoviejo</h4>
                                     <h2 class="uk-modal-title">Construyendo participación ciudadana en nuestros territorios</h2>
                                     <p>El equipo de Portoviejo impulsó instancias abiertas para que la ciudadanía evaluara las políticas de transparencia y acceso a la información. Estas instancias permitieron identificar una percepción negativa de la comunidad en torno a (1) información disponible y (2) espacios participativos vinculantes, generando pocos incentivos para el compromiso de las personas.</p>
                                     <p>Como solución, el equipo de servidores públicos realizó una serie de solicitudes al gobierno local, y consolidó el compromiso de los actores involucrados una serie de actividades participativas vinculantes. Además, el equipo comenzó un proceso de investigación en conjunto con su comunidad, para comprender las brechas de participación que enfrentan de grupos excluidos sistemáticamente.</p>

                                     <div class="video">
                                     </div>

                                     <div class="carrousel-fotos">            
                                     </div>


                                 </div>
                             </div>

                              </div>
                         </div>
                     </li>-->






                         <li>
                             <div class="uk-card uk-card-default uk-padding-small uk-padding-remove-left uk-padding-remove-right">
                                <div class="uk-card-media-top uk-margin-left uk-margin-right">
                                   <p class="animated fadeInUp delay-1s fast pais uk-text-center">México</p>
                                   <h4 class="animated fadeInUp delay-1s fast uk-margin-remove-bottom nombre-municipio uk-text-center">Veracruz</h4>
                                 </div>
                                 <div class="uk-card-body uk-margin-top uk-margin-bottom">
                                     <div class="img-card animated fadeInUp delay-1s fast">
                                         <img src="/assets/images/banners/portada-veracruz.png" alt="img municipio" class="img-municipio" width="100%">
                                         <div class="filtrer-gradient"></div>
                                     </div>
                                 </div>
                                 <div class="uk-card-media-bottom uk-margin-left uk-margin-right">
                                     <h3 class="animated fadeInUp delay-1s fast uk-text-bold uk-margin-remove-top uk-text-center titulo-experiencia">Atención municipal, innovación y sustentabilidad para la ciudadanía</h3>
                                     <a href="#modal-example-experiencia-1" uk-toggle class="btn animated fadeInUp delay-1s fast">Conoce la historia</a>
                                 <!-- This is the modal -->
                                 <div id="modal-example-experiencia-1" uk-modal>
                                     <div class="uk-modal-dialog uk-modal-body modal-experiencias">
                                         <button class="uk-modal-close-default" type="button" uk-close></button>
                                         <p class="pais">México</p>
                                         <h4 class="nombre-municipio">Veracruz</h4>
                                         <h2 class="uk-modal-title">Atención municipal, innovación y sustentabilidad para la ciudadanía</h2>
                                         <p>Durante el 2020 el municipio de Veracruz participó en Abre Alcaldías para profundizar en su eje institucional “Te Quiero en Marcha” que buscó “eficientar los servicios de la administración municipal priorizando la atención ciudadana; simplificando los trámites y servicios con mecanismos de innovación y sustentabilidad; realizando reducciones en el gasto, y transparentado el uso de los recursos públicos como un gobierno abierto y cercano a su ciudadanía”</p>
                                         <p>Para esto, se generaron acciones que buscaran facilitar el acceso y la difusión de información pública a la par de la implementación de mecanismos de participación ciudadana para recepción de opiniones y propuestas.</p>
                                         <div class="video">
                                             <iframe width="100%" height="350" src="https://www.youtube.com/embed/b-jh1o3lOAs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                                         </div>
                                         <div class="carrousel-fotos">

                                         </div>

                                     </div>
                                 </div>

                                  </div>
                             </div>
                         </li>

                         <li>
                             <div class="uk-card uk-card-default uk-padding-small uk-padding-remove-left uk-padding-remove-right">
                                <div class="uk-card-media-top uk-margin-left uk-margin-right">
                                   <p class="animated fadeInUp delay-1s fast pais uk-text-center">México</p>
                                   <h4 class="animated fadeInUp delay-1s fast uk-margin-remove-bottom nombre-municipio uk-text-center">San Luis de Potosí</h4>
                                 </div>
                                 <div class="uk-card-body uk-margin-top uk-margin-bottom">
                                     <div class="img-card animated fadeInUp delay-1s fast">
                                         <img src="/assets/images/banners/portada-sanluisdepotosi.png" alt="img municipio" class="img-municipio" width="100%">
                                         <div class="filtrer-gradient"></div>
                                     </div>
                                 </div>
                                 <div class="uk-card-media-bottom uk-margin-left uk-margin-right">
                                     <h3 class="animated fadeInUp delay-1s fast uk-text-bold uk-margin-remove-top uk-text-center titulo-experiencia">Tramitaciones digitales más eficientes y transparentes</h3>
                                     <a href="#modal-example-experiencia-2" uk-toggle class="btn animated fadeInUp delay-1s fast">Conoce la historia</a>
                                 <!-- This is the modal -->
                                 <div id="modal-example-experiencia-2" uk-modal>
                                     <div class="uk-modal-dialog uk-modal-body  modal-experiencias">
                                         <button class="uk-modal-close-default" type="button" uk-close></button>
                                         <p class="pais">México</p>
                                         <h4 class="nombre-municipio">San Luis de Potosí</h4>
                                         <h2 class="uk-modal-title">Tramitaciones digitales más eficientes y transparentes</h2>
                                         <p>El equipo del ayuntamiento de Potosí trabajó durante 2020 en dos iniciativas para mejorar la calidad de vida de sus vecinos y vecinas.</p>
                                         <p>La primera es el "Visor urbano", que ofrece a la ciudadanía acceder a información sobre las obras en construcción, rutas de aseo, consultar licencias de funcionamiento, ubicación de fallas geológicas y permite agilizar las gestiones de licencias de construcción de obras que no excedan los 1.500 mt2 y que se encuentren reguladas en las condicionantes de Uso de Suelo definidas por el municipio, todo a través del espacio virtual.</p>
                                         <p>La segunda es la App "Gobierno Municipal de San Luis" la cual cuenta con 7 módulos de servicios en los que las personas pueden acceder a información sobre el pago predial, atención ciudadana, pago de multas y consultas de servicios de tránsito, entre otras.
                                         </p>
                                         <div class="video">
                                             <iframe width="100%" height="350" src="https://www.youtube.com/embed/wd1E7m2Lt24" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                                         </div>
                                         <div class="carrousel-fotos">

                                         </div>

                                     </div>
                                 </div>

                                  </div>
                             </div>
                         </li>

                         <li>
                             <div class="uk-card uk-card-default uk-padding-small uk-padding-remove-left uk-padding-remove-right">
                                <div class="uk-card-media-top uk-margin-left uk-margin-right">
                                   <p class="animated fadeInUp delay-1s fast pais uk-text-center">México</p>
                                   <h4 class="animated fadeInUp delay-1s fast uk-margin-remove-bottom nombre-municipio uk-text-center">León, Guanajuato</h4>
                                 </div>
                                 <div class="uk-card-body uk-margin-top uk-margin-bottom">
                                     <div class="img-card animated fadeInUp delay-1s fast">
                                         <img src="/assets/images/banners/portada-leon-guanajuato.png" alt="img municipio" class="img-municipio" width="100%">
                                         <div class="filtrer-gradient"></div>
                                     </div>
                                 </div>
                                 <div class="uk-card-media-bottom uk-margin-left uk-margin-right">
                                     <h3 class="animated fadeInUp delay-1s fast uk-text-bold uk-margin-remove-top uk-text-center titulo-experiencia">Construcción participativa de la comunidad y sus áreas verdes</h3>
                                     <a href="#modal-example-experiencia-3" uk-toggle class="btn animated fadeInUp delay-1s fast">Conoce la historia</a>
                                 <!-- This is the modal -->
                                 <div id="modal-example-experiencia-3" uk-modal>
                                     <div class="uk-modal-dialog uk-modal-body  modal-experiencias">
                                         <button class="uk-modal-close-default" type="button" uk-close></button>
                                         <p class="pais">México</p>
                                         <h4 class="nombre-municipio">León, Guanajuato</h4>
                                         <h2 class="uk-modal-title">Construcción participativa de la comunidad y sus áreas verdes</h2>
                                         <p>El equipo de LAB León, en conjunto con el gobierno local, avanzaron durante el 2020 en proyectos de co-diseño de espacios públicos de manera participativa y vinculante.</p>
                                         <p>Con el objetivo de lograr que todas las personas que habitan en el ayuntamiento se sientan parte del espacio y se hagan responsable de su cuidado, logrando así un uso más eficiente de los recursos.</p>
                                         <p>La certeza es clara: si trabajamos juntos y juntas por crear, implementar y cuidar los espacios, haremos mejores municipios.</p>

                                         <div class="video">

                                         </div>

                                         <div class="carrousel-fotos">

                                         <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow>
                                             <ul class="uk-slideshow-items">
                                                 <li>
                                                     <img src="/assets/images/banners/leon-foto-1.png" alt="img municipio" class="img-municipio" width="100%" uk-cover>
                                                 </li>
                                                 <li>
                                                     <img src="/assets/images/banners/leon-foto-2.png" alt="img municipio" class="img-municipio" width="100%" uk-cover>
                                                 </li>
                                                 <li>
                                                     <img src="/assets/images/banners/leon-foto-3.png" alt="img municipio" class="img-municipio" width="100%" uk-cover>
                                                 </li>
                                                 <li>
                                                     <img src="/assets/images/banners/leon-foto-4.png" alt="img municipio" class="img-municipio" width="100%" uk-cover>
                                                 </li>
                                             </ul>

                                             <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"> <</a>
                                             <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next">  > </a>
                                         </div>

                                         </div>

                                     </div>
                                 </div>

                                  </div>
                             </div>
                         </li>


                         <li>
                             <div class="uk-card uk-card-default uk-padding-small uk-padding-remove-left uk-padding-remove-right">
                                <div class="uk-card-media-top uk-margin-left uk-margin-right">
                                   <p class="animated fadeInUp delay-1s fast pais uk-text-center">México</p>
                                   <h4 class="animated fadeInUp delay-1s fast uk-margin-remove-bottom nombre-municipio uk-text-center">Benito Juárez, Quintana Roo</h4>
                                 </div>
                                 <div class="uk-card-body uk-margin-top uk-margin-bottom">
                                     <div class="img-card animated fadeInUp delay-1s fast">
                                         <img src="/assets/images/banners/portada-benitojuarez.png" alt="img municipio" class="img-municipio" width="100%">
                                         <div class="filtrer-gradient"></div>
                                     </div>
                                 </div>
                                 <div class="uk-card-media-bottom uk-margin-left uk-margin-right">
                                     <h3 class="animated fadeInUp delay-1s fast uk-text-bold uk-margin-remove-top uk-text-center titulo-experiencia">Servicios básicos y asentamientos informales</h3>
                                     <a href="#modal-example-experiencia-4" uk-toggle class="btn animated fadeInUp delay-1s fast">Conoce la historia</a>
                                 <!-- This is the modal -->
                                 <div id="modal-example-experiencia-4" uk-modal>
                                     <div class="uk-modal-dialog uk-modal-body  modal-experiencias">
                                         <button class="uk-modal-close-default" type="button" uk-close></button>
                                         <p class="pais">México</p>
                                         <h4 class="nombre-municipio">Benito Juárez, Quintana Roo</h4>
                                         <h2 class="uk-modal-title">Servicios básicos, calidad de vida, participación ciudadana y asentamientos informales</h2>
                                         <p>Durante el 2020 el equipo de Benito Juárez, Quintana Roo quiso enfrentar un grave problema: la existencia de asentamientos irregulares que, al no ser “municipalizados”, no cuentan con los servicios básicos que suele brindar el gobierno local.</p>
                                         <p>Ante este escenario, el equipo se organizó y comenzó el proceso para implementar un presupuesto participativo de manera integral y enfocado especialmente a estos fraccionamientos no municipalizados con un claro objetivo: abordar la falta de servicios de zonas habitacionales sin drenaje y sin iluminación.</p>
                                         <p>Pero aún hay más. El segundo desafío que se propuso el municipio es simplificar los trámites administrativos en torno a la Licencia de Funcionamiento para establecimientos comerciales, industriales o de servicio. Esto mediante un proceso de mejora regulatorio, colaborativo, participativo y con enfoque en la ciudadanía, para construir procesos más eficientes, seguros y económicos.</p>
                                         <div class="video">
                                             <iframe width="100%" height="350" src="https://www.youtube.com/embed/jwTgpuuBxDA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                                         </div>
                                         <div class="carrousel-fotos">

                                         </div>

                                     </div>
                                 </div>

                                  </div>
                             </div>
                         </li>



                     </ul>

                 </div>
                 <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"> < </a>
                 <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"> > </a>

                <!--  <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul> -->
             </div>
             </div>

         </div>
         <div class="map">
             <img src="/assets/images/abre/mapa-abre.png" alt="img mapa" class="img-mapa" width="100%">
         </div>

    </div>
</section>


<!--<section id="texto-destacado-celeste">
    <div class="uk-container uk-container-medium uk-padding">
         <div class="uk-width-1 uk-flex uk-flex-center uk-flex-around@s uk-flex-middle uk-flex-wrap">
             <h2 class="animated fadeInUp delay-1s fast uk-text-center">Gobernanzas más abiertas, participativas e innovadoras</h2>
         </div>
    </div>
</section> -->


<!--<div class="bg-testimonios">
<section class="uk-container uk-container-medium uk-margin" id="testimonios">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">Testimonios</h3>
            <p class="sub-titulo animated fadeInUp delay-1s fast uk-text-center">Hoy forman parte de nuestra Red de Innovadores Municipales</p>
        </div>
    </div>
</section>


<section class="uk-container uk-container-large version-desktop" id="testimonios-fotos">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>

        <div class="uk-width-1@m uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <img src="/assets/images/banners/testimonio-1-img.png" alt="img persona" class="img-persona-testimonio animated fadeIn delay-1s fast" width="100%">
            <h4 class="uk-text-center animated fadeInUp delay-1s fast">Carla García</h4>
        </div>

        <div class="uk-width-1@s uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
          <img src="/assets/images/banners/testimonio-3-img.png" alt="img persona" class="img-persona-testimonio animated fadeIn delay-1s fast" width="100%">
          <h4 class="uk-text-center animated fadeInUp delay-1s fast">Esmeralda Ramos</h4>
        </div>

        <div class="uk-width-1@s uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <img src="/assets/images/banners/testimonio-2-img.png" alt="img persona" class="img-persona-testimonio animated fadeIn delay-1s fast" width="100%">
            <h4 class="uk-text-center animated fadeInUp delay-1s fast">Roberto Díaz</h4>
        </div>

        <div class="uk-width-1@s uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <img src="/assets/images/banners/testimonio-4-img.png" alt="img persona" class="img-persona-testimonio animated fadeIn delay-1s fast" width="100%">
            <h4 class="uk-text-center animated fadeInUp delay-1s fast">Damara Tejeda</h4>
        </div>

    </div>
</section>
</div>

<section class="uk-container uk-container-large version-desktop" id="testimonios-texto">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>

        <div class="uk-width-1@m uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <p class="profesion uk-text-center animated fadeInUp delay-1s fast">Directora unidad de transparencia</p>
            <p class="lugar uk-text-center animated fadeInUp delay-1s fast">Benito Juarez, México</p>
            <p class="mensaje uk-text-center animated fadeInUp delay-1s fast">"Un curso muy integral que aporta herramientas de trabajo en equipo, como dinámicas de trabajo y colaboración al exterior, con la sociedad" </p>
        </div>

        <div class="uk-width-1@s uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <p class="profesion uk-text-center animated fadeInUp delay-1s fast">Dirección de innovación tecnológica</p>
            <p class="lugar uk-text-center animated fadeInUp delay-1s fast">San Luis de Potosí, México</p>
            <p class="mensaje uk-text-center animated fadeInUp delay-1s fast">“Te permite formar parte de una red de otras alcaldías, que permite que podamos innovar y colaborar con otras personas”</p>
        </div>

        <div class="uk-width-1@s uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <p class="profesion uk-text-center animated fadeInUp delay-1s fast">Técnico en participación Ciudadana</p>
            <p class="lugar uk-text-center animated fadeInUp delay-1s fast">Ibarra, Ecuador</p>
            <p class="mensaje uk-text-center animated fadeInUp delay-1s fast">“Abre Alcaldías es una plataforma de información que te permite generar gobiernos locales más amplios, con más conocimiento”</p>
        </div>

        <div class="uk-width-1@s uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <p class="profesion uk-text-center animated fadeInUp delay-1s fast">Desarrolladora</p>
            <p class="lugar uk-text-center animated fadeInUp delay-1s fast">Veracruz, México</p>
            <p class="mensaje uk-text-center animated fadeInUp delay-1s fast">“Un espacio que fomenta y promueve los mecanismos y herramientas de gobierno abierto”</p>
        </div>

    </div>
</section>


<section class="uk-container uk-container-large version-movil" id="testimonios-fotos">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1@m uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <img src="/assets/images/banners/testimonio-1-img.png" alt="img persona" class="img-persona-testimonio animated fadeIn delay-1s fast" width="100%">
            <h4 class="uk-text-center animated fadeInUp delay-1s fast">Carla García</h4>
        </div>
    </div>
</section>
<section class="uk-container uk-container-large version-movil" id="testimonios-texto">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>

        <div class="uk-width-1@m uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <p class="profesion uk-text-center animated fadeInUp delay-1s fast">Directora unidad de transparencia</p>
            <p class="lugar uk-text-center animated fadeInUp delay-1s fast">Benito Juarez, México</p>
            <p class="mensaje uk-text-center animated fadeInUp delay-1s fast">"Un curso muy integral que aporta herramientas de trabajo en equipo, como dinámicas de trabajo y colaboración al exterior, con la sociedad" </p>
        </div>
    </div>
</section>

<section class="uk-container uk-container-large version-movil" id="testimonios-fotos">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1@m uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
           <img src="/assets/images/banners/testimonio-2-img.png" alt="img persona" class="img-persona-testimonio animated fadeIn delay-1s fast" width="100%">
           <h4 class="uk-text-center animated fadeInUp delay-1s fast">Esmeralda Ramos</h4>
        </div>
    </div>
</section>
<section class="uk-container uk-container-large version-movil" id="testimonios-texto">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>

        <div class="uk-width-1@m uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <p class="profesion uk-text-center animated fadeInUp delay-1s fast">Dirección de innovación tecnológica</p>
            <p class="lugar uk-text-center animated fadeInUp delay-1s fast">San Luis de Potosí, México</p>
            <p class="mensaje uk-text-center animated fadeInUp delay-1s fast">“Te permite formar parte de una red de otras alcaldías, que permite que podamos innovar y colaborar con otras personas”</p>
        </div>
    </div>
</section>

<section class="uk-container uk-container-large version-movil" id="testimonios-fotos">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1@m uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
           <img src="/assets/images/banners/testimonio-3-img.png" alt="img persona" class="img-persona-testimonio animated fadeIn delay-1s fast" width="100%">
           <h4 class="uk-text-center animated fadeInUp delay-1s fast">Roberto Díaz</h4>
        </div>
    </div>
</section>
<section class="uk-container uk-container-large version-movil" id="testimonios-texto">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>

        <div class="uk-width-1@m uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
           <p class="profesion uk-text-center animated fadeInUp delay-1s fast">Técnico en participación Ciudadana</p>
           <p class="lugar uk-text-center animated fadeInUp delay-1s fast">Ibarra, Ecuador</p>
           <p class="mensaje uk-text-center animated fadeInUp delay-1s fast">“Abre Alcaldías es una plataforma de información que te permite generar gobiernos locales más amplios, con más conocimiento”</p>
        </div>
    </div>
</section>

<section class="uk-container uk-container-large version-movil" id="testimonios-fotos">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1@m uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
            <img src="/assets/images/banners/testimonio-4-img.png" alt="img persona" class="img-persona-testimonio animated fadeIn delay-1s fast" width="100%">
            <h4 class="uk-text-center animated fadeInUp delay-1s fast">Damara Tejeda</h4>
        </div>
    </div>
</section>
<section class="uk-container uk-container-large version-movil" id="testimonios-texto">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>

        <div class="uk-width-1@m uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
           <p class="profesion uk-text-center animated fadeInUp delay-1s fast">Desarrolladora</p>
           <p class="lugar uk-text-center animated fadeInUp delay-1s fast">Veracruz, México</p>
          <p class="mensaje uk-text-center animated fadeInUp delay-1s fast">“Un espacio que fomenta y promueve los mecanismos y herramientas de gobierno abierto”</p>
        </div>
    </div>
</section> -->




<div class="fondo-herramientas">
<section class="uk-container uk-container-medium uk-margin" id="herramientas">
    <div class="uk-flex uk-flex-left" uk-grid>
        <div class="uk-width-1">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">Publicaciones</h3>
        </div>


        <div class="uk-width-1@s uk-width-1-3@m herramienta-box">
            <img src="/assets/images/publicacion-1.png" alt="img" class="img-herramienta" width="100%">
            <div class="descripcion">
               <!--<h3 class="animated fadeInUp delay-1s fast uk-text-center">DemocraciaKit</h3>-->
               <a href="https://drive.google.com/file/d/1kH0MKyZMcqpYCVCNjlYOiQnE9GeU5Gay/view" target="_blank" class="btn">Descargar</a>
            </div>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m herramienta-box">
            <img src="/assets/images/publicacion-2.png" alt="img" class="img-herramienta" width="100%">
            <div class="descripcion">
               <!--<h3 class="animated fadeInUp delay-1s fast uk-text-center">Manual Abre Alcaldías</h3>-->
               <a href="https://drive.google.com/file/d/1l9rRNGLfrxYm-bzTBPYzd4XQD2U8aV6E/view" target="_blank" class="btn">Descargar</a>
            </div>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m herramienta-box">
            <img src="/assets/images/publicacion-3.png" alt="img" class="img-box" width="100%">
            <div class="descripcion">
               <!--<h3 class="animated fadeInUp delay-1s fast uk-text-center">Iniciativas de participación en gobierno local</h3>-->
               <a href="https://drive.google.com/file/d/18I6Q3H0pIY8qSxsyssoZxQzz5FZPcKh6/view?usp=drive_link" target="_blank" class="btn">Descargar</a>
            </div>
        </div>


    </div>
</section>
</div>


<section id="org-project">
    <div class="uk-container uk-container-xlarge uk-margin-large-top uk-margin-large-bottom">
         <div class="uk-width-1">
             <h4 class="animated fadeInUp delay-1s fast uk-text-center">Colaboran</h4>
             <div class="animated fadeInUp delay-1s fast uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider>
               <div>
                 <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-5@s uk-child-width-1-6@m">
                 <li>
                     <img src="/assets/images/banners/amaranta-logo.png" width="400" height="600" alt="logo">
                 </li>
                 <li>
                     <img src="/assets/images/banners/fima-logo.png" width="400" height="600" alt="logo">
                 </li>
                 <li>
                     <img src="/assets/images/banners/logo-3.png" width="400" height="600" alt="logo">
                 </li>

                     <li>
                         <img src="/assets/images/abre/techo-logo.png" width="400" height="600" alt="logo">
                     </li>
                     <li>
                         <img src="/assets/images/abre/red-ciudadana-logo.png" width="400" height="600" alt="logo">
                     </li>
                     <li>
                         <img src="/assets/images/abre/pares-logo.png" width="400" height="600" alt="logo">
                     </li>
                     <li>
                         <img src="/assets/images/abre/oppg-logo.png" width="400" height="600" alt="logo">
                     </li>
                     <li>
                         <img src="/assets/images/abre/ollin-logo.png" width="400" height="600" alt="logo">
                     </li>

                     <li>
                         <img src="/assets/images/abre/no-ficcion-logo.png" width="400" height="600" alt="logo">
                     </li>

                     <li>
                         <img src="/assets/images/abre/mexiro-logos.png" width="400" height="600" alt="logo">
                     </li>

                     <li>
                         <img src="/assets/images/abre/extituto-logo.png" width="400" height="600" alt="logo">
                     </li>

                     <li>
                         <img src="/assets/images/abre/escuela-mexicana-logo.png" width="400" height="600" alt="logo">
                     </li>

                     <li>
                         <img src="/assets/images/abre/asuntosdelsur-logo.png" width="400" height="600" alt="logo">
                     </li>

                     <li>
                         <img src="/assets/images/abre/accion-logo.png" width="400" height="600" alt="logo">
                     </li>

                     <li>
                         <img src="/assets/images/abre/accion-ciudadana-logo.png" width="400" height="600" alt="logo">
                     </li>

                 </ul>

                 <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"> < </a>
                 <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"> > </a>
                 </div>

             </div>

         </div>
    </div>
</section>


<!--<div class="img-grupo">
    <img src="/assets/images/banners/textura-circulos.png" alt="img textura circulos" class="img-textura" width="100%">
    <img src="/assets/images/banners/img-grupo-1.png" alt="img grupo abre alcaldías" class="img" width="100%">
</div> -->

<!--<div class="proyecto-destacado">
    <h3 class="animated fadeInUp delay-1s fast uk-text-center">Proyecto destacado</h3>
    <p class="sub-titulo animated fadeInUp delay-1s fast uk-text-center">en el Premio Latinoamericano Democracia Digital 2023 </p>
    <p class="sub-titulo-2 animated fadeInUp delay-1s fast uk-text-center">en apertura de datos y transparencia</p>
</div>-->

<script>
UIkit.slider(element).show(index);
</script>
