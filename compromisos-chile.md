---
layout: page-compromisos-chile
sub-title:
title: Compromisos en Chile
permalink: /compromisos-chile/
welcome_desktop: ../assets/images/banner-compromisos-abre.png
welcome_mobile: ../assets/images/banner-compromisos-abre.png
---


<div class="compromisos-chile-title uk-container uk-container-small uk-margin">
     <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
         <h1 class="animated fadeInUp delay-1s fast">Conoce los 5 Compromisos</h1>
         <p class="animated fadeInUp delay-1s fast">Para impulsar <b>la participación ciudadana, la equidad de género, la justicia ambiental</b> y el acceso equitativo a la información en Chile.</p>
    </div>
</div>

<div class="bg-proceso-formacion">
<section class="uk-container uk-container-small uk-margin" id="proceso-formacion">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">

        <div class="paso">
          <div class="titulo">
             <h3 class="animated fadeInUp delay-1s fast uk-text-center">1</h3>
             <h4 class="animated fadeInUp delay-1s fast uk-text-center">Participación Ciudadana Permanente</h4>
          </div>
          <div class="descripcion animated fadeInUp delay-1s fast">
             <p>Promover la participación continua de la ciudadanía en la toma de decisiones.</p>
          </div>
          </div>

          <div class="paso">
            <div class="titulo">
               <h3 class="animated fadeInUp delay-1s fast uk-text-center">2</h3>
               <h4 class="animated fadeInUp delay-1s fast uk-text-center">Gobernanza Abierta y Transparente</h4>
            </div>
            <div class="descripcion animated fadeInUp delay-1s fast">
               <p>Adoptar medidas de transparencia proactivas, entregando información y promoviendo el monitoreo ciudadano.</p>
            </div>
            </div>

            <div class="paso">
              <div class="titulo">
                 <h3 class="animated fadeInUp delay-1s fast uk-text-center">3</h3>
                 <h4 class="animated fadeInUp delay-1s fast uk-text-center">Equidad de Género e Inclusión Intercultural</h4>
              </div>
              <div class="descripcion animated fadeInUp delay-1s fast">
                 <p>Impulsar y apoyar iniciativas que buscan la participación igualitaria, promoviendo espacios seguros, libres de violencia y abiertos a todas las personas en la comuna.</p>
              </div>
              </div>

              <div class="paso">
                <div class="titulo">
                   <h3 class="animated fadeInUp delay-1s fast uk-text-center">4</h3>
                   <h4 class="animated fadeInUp delay-1s fast uk-text-center">Sostenibilidad y Justicia Ambiental</h4>
                </div>
                <div class="descripcion animated fadeInUp delay-1s fast">
                   <p>Apoyar e impulsar iniciativas que busquen protección del medio ambiente y que promuevan la justicia ambiental, con foco en las comunidades más afectadas.</p>
                </div>
                </div>

                <div class="paso">
                  <div class="titulo">
                     <h3 class="animated fadeInUp delay-1s fast uk-text-center">5</h3>
                     <h4 class="animated fadeInUp delay-1s fast uk-text-center">Innovación y cercanía
                     </h4>
                  </div>
                  <div class="descripcion animated fadeInUp delay-1s fast">
                     <p>Facilitar la información a través de canales e instancias accesibles y abiertas, promoviendo la cercanía y la transparencia en la gestión pública.</p>
                  </div>
                  </div>


        </div>
    </div>
</section>
</div>


<!--Firma-->
<section class="bg-gris-yoabromialcaldia">
  <div class="uk-container uk-container-small uk-margin">
     <div class="uk-flex uk-flex-center uk-flex-middle alinear-yoabromialcaldia" uk-grid>
         <h2 class="animated fadeInUp delay-1s fast">¡Firma el compromiso hoy!</h2>
         <p class="animated fadeInUp delay-1s fast">Hazte parte de la construcción de territorios más <b>participativos, abiertos e inclusivos.</b></p>
         <a href="https://forms.gle/n9Ba6ndEAaKhhhaD7" target="_blank" class="boton-abre-alcaldias">Firma Aquí</a>
    </div>
  </div>
</section>


<!--compartir en rrss-->
<section class="comparte-rrss-yoabromialcaldia">
  <div class="uk-container uk-container-small uk-margin">
     <div class="uk-flex uk-flex-center uk-flex-middle alinear-yoabromialcaldia" uk-grid>
         <h3 class="animated fadeInUp delay-1s fast">¿No eres candidato/a? ¡También puedes participar! </h3>
         <p class="animated fadeInUp delay-1s fast"><b>Comparte en redes sociales y etiqueta a tus candidatos</b> para que firmen el compromiso.</p>
    </div>
  </div>
</section>

<!--mensaje destacado-->
<section class="mensaje-destacado-yoabromialcaldia">
  <div class="uk-container uk-container-small uk-margin">
     <div class="uk-flex uk-flex-center uk-flex-middle alinear-yoabromialcaldia" uk-grid>
         <h2 class="animated fadeInUp delay-1s fast">Construyamos juntos una democracia más<br><span>inclusiva, innovadora y sostenible</span></h2>
    </div>
  </div>
</section>