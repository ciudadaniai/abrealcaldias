---
layout: page-red-de-innovadores
sub-title: Cómo trabajamos
title: Red de innovadores
permalink: /red-de-innovadores/
welcome_desktop: ../assets/images/banners/banner-como-trabajamos.png
welcome_mobile: ../assets/images/banners/banner-como-trabajamos.png
---

<!--<div class="uk-section section-no-color animated fadeIn delay-1s fast">
  <div class="uk-container uk-width-3-4@m" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
      <div class="uk-width-3-4@m">
      <h2>{% t about_us.title %}</h2>
      <p>{% t about_us.description_1 %}</p>
      <p>{% t about_us.description_2 %}</p>
      </div>
      <div class="uk-width-1-4@m">
      </div>
    </div>
  </div>
</div>

<div class="uk-section section-color-2nd animated fadeIn">
  <div class="uk-container uk-width-1-1" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-2-3@m uk-text-center ">
        <h2>{% t about_us.alliances.title %}</h2>
        <p>{% t about_us.alliances.description %}</p>
      </div>
    </div>
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <div class="uk-flex uk-flex-center" uk-grid>
          <div class="uk-child-width-1-1 uk-child-width-1-4@m uk-grid-small uk-flex uk-flex-center" uk-grid="masonry: true">
            <div>
              <div class="uk-card uk-card-default">
                <div class="uk-card-media-top">
                  <img src="/assets/images/logo-ecuador.png" alt="Logo {% t about_us.org_1.title %}">
                </div>
                <div class="uk-card-body">
                  <p class="font-little-title" style="margin-bottom: -20px;">{% t about_us.org_1.subtitle %}</p>
                  <h4 class="no-project-color">{% t about_us.org_1.title %}</h4>
                  <dl class="uk-description-list">
                    <dt class="no-project-color">{% t about_us.org_1.issues %}</dt>
                    <dd class="font-compressed">{% t about_us.org_1.description %}</dd>
                  </dl>
                  <a href="https://oppguayaquil.org/"><p class="font-mono font-small">{% t about_us.org_1.action %}</p></a>
                </div>
              </div>        
            </div>  
            <div>
              <div class="uk-card uk-card-default">
                <div class="uk-card-media-top">
                  <img src="/assets/images/logo-mexico.png" alt="Logo {% t about_us.org_2.title %}">
                </div>
                <div class="uk-card-body">              
                  <p class="font-little-title" style="margin-bottom: -20px;">{% t about_us.org_2.subtitle %}</p>
                  <h4 class="no-project-color">{% t about_us.org_2.title %}</h4>
                  <dl class="uk-description-list">
                      <dt class="no-project-color">{% t about_us.org_2.issues %}</dt>
                      <dd class="font-compressed">{% t about_us.org_2.description %}</dd>
                  </dl>
                  <a href="http://www.mexiro.org/"><p class="font-mono font-small">{% t about_us.org_2.action %}</p></a>
                </div>
              </div>       
            </div>
            <div>
              <div class="uk-card uk-card-default">
                <div class="uk-card-media-top">
                  <img src="/assets/images/logo-guatemala.png" alt="Logo {% t about_us.org_3.title %}">
                </div>
                <div class="uk-card-body">
                  <p class="font-little-title" style="margin-bottom: -20px;">{% t about_us.org_3.subtitle %}</p>
                  <h4 class="no-project-color">{% t about_us.org_3.title %}</h4>
                  <dl class="uk-description-list">
                      <dt class="no-project-color">{% t about_us.org_3.issues %}</dt>
                      <dd class="font-compressed">{% t about_us.org_3.description %}</dd>
                  </dl>
                  <a href="https://www.no-ficcion.com/nosotros-as"><p class="font-mono font-small">{% t about_us.org_3.action %}</p></a>
                </div>      
              </div>    
            </div>
            <div>
              <div class="uk-card uk-card-default">
                <div class="uk-card-media-top">
                  <img src="/assets/images/logo-salvador.png" alt="Logo {% t about_us.org_4.title %}">
                </div>
                <div class="uk-card-body">
                  <p class="font-little-title" style="margin-bottom: -20px;">{% t about_us.org_4.subtitle %}</p>
                  <h4 class="no-project-color">{% t about_us.org_4.title %}</h4>
                  <dl class="uk-description-list">
                      <dt class="no-project-color">{% t about_us.org_2.issues %}</dt>
                      <dd class="font-compressed">{% t about_us.org_4.description %}</dd>
                  </dl>
                </div>
              </div>        
            </div>                                                
          </div>
        </div>
      </div>
    </div>
  </div>
</div>-->
