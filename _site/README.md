## Instalación

- Install jekyll
```
gem install bundler jekyll
```
- Clone repository
```
git@gitlab.com:ciudadaniai/abrealcaldias.git
```
- ` cd abrealcaldias` to enter the site
- ` bundle install` to install the gems
- Ready!
```
bundle exec jekyll serve --watch --baseurl=    
```
