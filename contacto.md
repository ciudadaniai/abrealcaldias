---
layout: page-contacto
sub-title:
title: Contacto
permalink: /contacto/
welcome_desktop: ../assets/images/banners/banner-contacto.png
welcome_mobile: ../assets/images/banners/banner-contacto.png
---

<section class="uk-container uk-container-small uk-margin-large" id="contacto">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">¿Tienes dudas, preguntas o sugerencias?</h3>
            <p class="animated fadeInUp delay-1s fast uk-text-center">Llena este formulario y nuestro equipo se contactará contigo lo antes posible.</p>
            <form uk-grid>
               <div class="uk-width-1@s uk-width-1-2@m">
                  <input class="uk-input animated fadeInUp delay-1s fast" type="text" placeholder="Nombre">
               </div>
               <div class="uk-width-1@s uk-width-1-2@m">
                  <input class="uk-input animated fadeInUp delay-1s fast" type="text" placeholder="Apellido">
               </div>
               <div class="uk-width-1@s uk-width-1-2@m">
                  <select aria-label="Custom controls" class="animated fadeInUp delay-1s fast">
                      <option value="">Indicar según corresponda</option>
                      <option value="1">Servidor Público</option>
                      <option value="2">Activista</option>
                      <option value="3">Organización sociedad civil</option>
                   </select>
               </div>
               <div class="uk-width-1@s uk-width-1-2@m">
                  <input class="uk-input animated fadeInUp delay-1s fast" type="text" placeholder="Email">
               </div>
               <div class="uk-width-1">
                  <input class="uk-input animated fadeInUp delay-1s fast" type="text" placeholder="Asunto">
               </div>
               <div class="uk-width-1">
                  <textarea class="uk-textarea animated fadeInUp delay-1s fast" rows="5" placeholder="Mensaje"></textarea>
               </div>
               <div class="btn uk-width-1">
                  <button class="uk-button uk-button-default animated fadeInUp delay-1s fast">Enviar</button>
               </div>
           </form>
        </div>
   </div>
</section>

<!--<section id="preguntas-frecuentes">
<div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
    <div class="uk-width-1@s uk-width-1-2@m">
        <h2 class="animated fadeInUp delay-1s fast uk-text-center">Preguntas frecuentes</h2>
    </div>
</div>    

<ul uk-accordion class="animated fadeInUp delay-1s fast">
    <li class="uk-open">
        <a class="uk-accordion-title uk-text-bold" href="#">{% t preguntas_frecuentes.pregunta_1 %}</a>
        <div class="uk-accordion-content">
            <p>{% t preguntas_frecuentes.respuesta_1 %}</p>
        </div>
    </li>
    <li>
        <a class="uk-accordion-title uk-text-bold" href="#">{% t preguntas_frecuentes.pregunta_2 %}</a>
        <div class="uk-accordion-content">
            <p>{% t preguntas_frecuentes.respuesta_2 %}</p>
        </div>
    </li>
    <li>
        <a class="uk-accordion-title uk-text-bold" href="#">{% t preguntas_frecuentes.pregunta_3 %}</a>
        <div class="uk-accordion-content">
            <p><b>Formación general:</b> Se realizan cinco módulos de clases en formato virtual que dan una mirada integral y holística sobre el proceso de participación ciudadana en la toma de decisiones.</p>
            <ul>
                <li><p>Desconfianza institucional en Latinoamérica: Causas raíces</p></li>
                <li><p>Estándares y recomendaciones de gestión pública</p></li>
                <li><p>Diseño y evaluación de proyectos con enfoque participativo</p></li>
                <li><p>Procesos de innovación y metodologías</p></li>
                <li><p>Mecanismos de democracia directa ciudadana</p></li>

            </ul>

            <p><b>Diagnóstico local:</b> Se generan espacios donde los equipos de gobierno local y organizaciones de la sociedad civil evalúan su propio contexto, identificando metas de apertura y participación para su territorio.</p>

            <p><b>Planes personalizados:</b> El diagnóstico local realizado permite generar acciones de cambio personalizadas y alcanzables para cada territorio, favoreciendo la participación ciudadana y la co-construcción.</p>

            <p><b>Acompañamiento e intercambio:</b> Etapa de implementación de los planes personalizados, los cuales se complementan con espacios de encuentro e intercambio con experiencias de otros gobiernos locales y ciudadanía.</p>

            <p><b>Si tienes más dudas sobre este punto, revisa: <a href="{{ site.baseurl_root }}/proceso-de-formacion" class="enlace-fac">Cómo trabajamos</a></b></p>

        </div>
    </li>
    <li>
        <a class="uk-accordion-title uk-text-bold" href="#">{% t preguntas_frecuentes.pregunta_4 %}</a>
        <div class="uk-accordion-content">
            <p>Sí, Abre Alcaldías se realizó el año 2020 y 2021 en México, El Salvador, Guatemala y Ecuador, dando un total de 78 personas formadas. Puedes revisar algunas historias de éxito en: <a href="{{ site.baseurl_root }}/#" class="enlace-fac">Experiencias</a></p>
        </div>
    </li>
    <li>
        <a class="uk-accordion-title uk-text-bold" href="#">{% t preguntas_frecuentes.pregunta_5 %}</a>
        <div class="uk-accordion-content">
            <p>Somos una organización sin fines de lucro, latinoamericana que lucha por la justicia social y el fortalecimiento de las democracias. Con 13 años de experiencia en 14 países, investigamos los obstáculos para la participación política de la ciudadanía y aplicamos nuestros hallazgos para promover nuevos marcos institucionales, construir narrativas transformadoras, formar agentes de cambio y articular procesos de fortalecimiento y resiliencia democrática. Conoce más sobre esta organización en: <a href="https://ciudadaniai.org/" class="enlace-fac" target="_blank">https://ciudadaniai.org/</a>
            </p>
        </div>
    </li>
</ul>  
</section>


<section class="uk-container uk-margin-large" id="contacto-end">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <h4 class="animated fadeInUp delay-1s fast">¿Tienes preguntas adicionales sobre Abre Alcaldías 2022 o el proceso de solicitud?</h4>
            <p class="animated fadeInUp delay-1s fast">¡Completa la información de contacto!</p>
        </div>
    </div>
</section>-->
