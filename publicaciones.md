---
layout: page-publicaciones
sub-title:
title: Publicaciones
permalink: /publicaciones/
welcome_desktop: ../assets/images/banners/banner-herramientas.png
welcome_mobile: ../assets/images/banners/banner-herramientas.png
---

<div class="fondo-herramientas">
<section class="uk-container uk-container-medium uk-margin" id="herramientas">
    <div class="uk-flex uk-flex-left" uk-grid>
        <div class="uk-width-1">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">Creamos herramientas para el fortalecimiento de gobiernos locales</h3>
        </div>


        <div class="uk-width-1@s uk-width-1-3@m herramienta-box">
            <img src="/assets/images/publicacion-1.png" alt="img" class="img-herramienta" width="100%">
            <div class="descripcion">
               <!--<h3 class="animated fadeInUp delay-1s fast uk-text-center">DemocraciaKit</h3>-->
               <a href="https://drive.google.com/file/d/1kH0MKyZMcqpYCVCNjlYOiQnE9GeU5Gay/view" target="_blank" class="btn">Descargar</a>
            </div>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m herramienta-box">
            <img src="/assets/images/publicacion-2.png" alt="img" class="img-herramienta" width="100%">
            <div class="descripcion">
               <!--<h3 class="animated fadeInUp delay-1s fast uk-text-center">Manual Abre Alcaldías</h3>-->
               <a href="https://drive.google.com/file/d/1l9rRNGLfrxYm-bzTBPYzd4XQD2U8aV6E/view" target="_blank" class="btn">Descargar</a>
            </div>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m herramienta-box">
            <img src="/assets/images/publicacion-3.png" alt="img" class="img-box" width="100%">
            <div class="descripcion">
               <!--<h3 class="animated fadeInUp delay-1s fast uk-text-center">Iniciativas de participación en gobierno local</h3>-->
               <a href="https://drive.google.com/file/d/18I6Q3H0pIY8qSxsyssoZxQzz5FZPcKh6/view?usp=drive_link" target="_blank" class="btn">Descargar</a>
            </div>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m herramienta-box">
            <img src="/assets/images/publicacion-4.png" alt="img" class="img-box" width="100%">
            <div class="descripcion">
               <!--<h3 class="animated fadeInUp delay-1s fast uk-text-center">KIT para el trabajo colaborativo remoto</h3>-->
               <a href="https://drive.google.com/file/d/1dF5tb3INq3FHWD9I88pevkJ0pMjGJC1v/view?usp=drive_link" target="_blank" class="btn">Descargar</a>
            </div>
        </div>


                <div class="uk-width-1@s uk-width-1-3@m herramienta-box">
                    <img src="/assets/images/publicacion-5.png" alt="img" class="img-box" width="100%">
                    <div class="descripcion">
                       <!--<h3 class="animated fadeInUp delay-1s fast uk-text-center">KIT para el trabajo colaborativo remoto</h3>-->
                       <a href="https://drive.google.com/file/d/1NUyeLbzjuCKwtjmTcTIMTfiuGp5xBGOy/view?usp=drive_link" target="_blank" class="btn">Descargar</a>
                    </div>
                </div>


                        <div class="uk-width-1@s uk-width-1-3@m herramienta-box">
                            <img src="/assets/images/publicacion-6.png" alt="img" class="img-box" width="100%">
                            <div class="descripcion">
                              <!-- <h3 class="animated fadeInUp delay-1s fast uk-text-center">KIT para el trabajo colaborativo remoto</h3>-->
                               <a href="https://drive.google.com/file/d/1dTN-sZIME8oVL_3099fVnEqnixk45hyZ/view?usp=drive_link" target="_blank" class="btn">Descargar</a>
                            </div>
                        </div>

      <!--  <div class="uk-width-1 mensaje">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">La participación en Abre Alcaldías <span>no tiene costo</span></h3>
        </div> -->

    </div>
</section>
</div>

<!--<div class="img-grupo">
    <img src="/assets/images/banners/fondo-personas-2.png" alt="img grupo abre alcaldías" class="img" width="100%">
</div> -->

<!--<section id="cta-abre" class="bg-celeste">
    <div class="uk-container uk-container-small uk-padding">
         <div class="uk-width-1 uk-flex uk-flex-center uk-flex-around@s uk-flex-middle uk-flex-wrap">
             <h3 class="animated fadeInUp delay-1s fast uk-text-center uk-text-center@s uk-text-left@m"><span>Una red de innovadores</span><br>
municipales en América Latina</h3>

         </div>
    </div>
</section> -->
