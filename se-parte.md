---
layout: page
title: ¡Se parte!
permalink: /se-parte/
welcome_desktop: ../assets/images/abre/banner-como-trabajamos.png
welcome_mobile: ../assets/images/abre/banner-movil-3.png
---


<section class="uk-container uk-margin-large" id="se-parte">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <h4 class="animated fadeInUp delay-1s fast">Proceso actual</h4>
            <p class="animated fadeInUp delay-1s fast">Actualmente estamos trabajando con gobiernos de México, Colombia, Guatemala y Chile. Si quieres saber más de las iniciativas, puedes revisar nuestras redes sociales o contactarnos</p>
        </div>

        <div class="uk-width-1 btn uk-margin-medium">
           <a href="{{ site.baseurl_root }}/contacto" class="animated fadeInUp delay-1s fast btn-abre"> Contáctanos > </a>
        </div>

    </div>
</section>
