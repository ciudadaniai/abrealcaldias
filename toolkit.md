---
layout: page-toolkit
sub-title: ¡Muy pronto!
title: Toolkit Abre Alcaldías
permalink: /toolkit/
welcome_desktop: ../assets/images/toolkit/banner-toolkit.png
welcome_mobile: ../assets/images/toolkit/banner-toolkit.png
---

<div class="bg-que-es sobre-toolkit">

  <section class="uk-container uk-container-small uk-margin">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">Sobre el <span>Toolkit</span> de Abre Alcaldías</h3>
            <p class="animated fadeInUp delay-1s fast uk-text-center">Una recopilación innovadora e interactiva de los aprendizajes acumulados por el proyecto <b>AbreAlcaldías</b> a lo largo de sus 4 ciclos de implementación entre los años 2018-2023 en 6 países de América Latina y Caribe. </p>
        </div>
    </div>
</section>





<div class="a-quien-esta-dirigido">
<section class="uk-container uk-container-small" id="preguntas">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
    <div class="uk-width-1">
        <h3 class="animated fadeInUp delay-1s fast uk-text-center">¿A quien está <span>dirigido</span>?</h3>
    </div>


  <div class="uk-width-1">
        <ul uk-accordion="multiple: true">
            <li>
                <a class="uk-accordion-title" href="#">1. Funcionarios y funcionarias públicos</a>
                <div class="uk-accordion-content">
                    <p>Comprometidos con la promoción de políticas públicas innovadoras, sostenibles e inclusivas en Latinoamérica y el Caribe, con visión en el futuro, buscando obtener mejores resultados y eficiencia en su toma de decisión.</p>
                </div>
            </li>
            <li>
                <a class="uk-accordion-title" href="#">2. Organizaciones de la sociedad civil y liderazgos sociales</a>
                <div class="uk-accordion-content">
                    <p>Con interés en influir positivamente en sus territorios y gobiernos a través de propuestas y construcción de innovaciones colaborativas, o reformas políticas sobre las necesidades prioritárias de sus comunidades.</p>
                </div>
            </li>
        </ul>
   </div>

    </div>
</section>

</div>

</div>


<section class="uk-container uk-container uk-margin" id="que-encontrare">
<div class="uk-child-width-1 uk-flex-center uk-flex-top" uk-grid>

<div class="uk-width-1">
    <h3 class="animated fadeInUp delay-1s fast uk-text-center">¿Qúe encontraré?</h3>
    <h4 class="animated fadeInUp delay-1s fast uk-text-center">Una historia de cambio</h4>
</div>


<div class="uk-width-1@s uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
  <div class="item-toolkit bg-1">
  <h4 class="animated fadeInUp delay-1s fast">Herramientas</h4>
  <p class="animated fadeInUp delay-1s fast uk-text-center">Para el diseño e implementación de políticas públicas <b>participativas y con enfoque en derechos humanos</b>, permitiendo la construcción de respuestas innovadoras y eficaces.</p>
  </div>
</div>

<div class="uk-width-1@s uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
  <div class="item-toolkit bg-2">
  <h4 class="animated fadeInUp delay-1s fast">Inspiración</h4>
  <p class="animated fadeInUp delay-1s fast uk-text-center"><b>Casos de inspiración</b>, voces de agentes de cambios y reflexiones sobre la participación ciudadana, la apertura y la innovación, su relevancia en la gestión pública local y distintos tips para sobrellevar las barreras que se pueden presentar.</p>
  </div>
</div>

<div class="uk-width-1@s uk-width-1-4@m uk-flex uk-flex-center uk-flex-column uk-flex-align-center">
  <div class="item-toolkit bg-3">
  <h4 class="animated fadeInUp delay-1s fast">Jornada</h4>
  <p class="animated fadeInUp delay-1s fast uk-text-center">Un viaje a través de nuestro <b>Método de Intervención</b> desde el cual se desprenden recomendaciones para la innovación pública con enfoque en territorios.</p>
  </div>
</div>



</div>  
</section>




<section class="uk-container uk-container-small uk-margin-large inscribete-toolkit" id="contacto">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">Inscríbete</h3>
            <h4 class="animated fadeInUp delay-1s fast uk-text-center">y recibe más informaciones sobre su lanzamiento digital y presencial</h4>
        </div>

        <div class="uk-width-1 uk-flex uk-flex-center uk-flex-column uk-flex-align-center margin-none">
            <a href="https://forms.gle/1VaMtaNNKpocVMWL8" class="btn animated fadeInUp delay-1s fast" target="_blank">Inscripción aquí</a>
        </div>

   </div>
</section>





<div class="anuncio">

<section class="uk-container uk-container-small uk-margin" id="que-es">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1 items">
            <h3>Atención Colombia:</h3>
            <div class="box">
                <p class="animated fadeInUp delay-1s fast uk-text-center">En Agosto Abre Alcaldías llega a Bucaramanga para encontrarnos con liderazgos territoriales y funcionarios/as públicos con interés en apertura, participación ciudadana e innovación pública:</p>
                <p class="animated fadeInUp delay-1s fast uk-text-center">Entérate de más <a href="https://forms.gle/1VaMtaNNKpocVMWL8" target="_blank">inscribiéndote aquí</a></p>
            </div>
        </div>
    </div>
</section>
</div>

<div class="img-grupo">
    <img src="/assets/images/toolkit/img-toolkit-footer.png" alt="img" class="img" width="100%">
</div>
