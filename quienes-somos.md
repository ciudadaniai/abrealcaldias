---
layout: page-quienes-somos
sub-title:
title: ¿Quiénes somos?
permalink: /quienes-somos/
welcome_desktop: ../assets/images/banners/banner-el-proyecto.png
welcome_mobile: ../assets/images/banners/banner-el-proyecto.png
---


<div class="bg-que-es">

<section class="uk-container uk-container-small uk-margin" id="que-es">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <p class="que-es-descripcion animated fadeInUp delay-1s fast uk-text-center">Abre Alcaldías es un proyecto de Fundación Ciudadanía Inteligente que desde 2018 trabaja junto a la gestión pública local de municipios latinoamericanos para impulsar metodologías y herramientas que potencian políticas públicas más innovadoras, transparentes y representativas.</p>
        </div>
    </div>
</section>


<section class="uk-container uk-container-small uk-margin" id="descripciones">
<div class="uk-child-width-1" uk-grid>
    <div>
        <div uk-grid class="descripcion animated fadeInUp delay-1s fast">
            <div class="uk-width-auto@m">
                <ul class="uk-tab-left" uk-tab="connect: #component-tab-left; animation: uk-animation-fade">
                    <li><a href="#">Visión</a></li>
                    <li><a href="#">Objetivos</a></li>
                    <!--<li><a href="#">¿Cómo lo hacemos?</a></li> -->
                </ul>
            </div>
            <div class="uk-width-expand@m">
                <ul id="component-tab-left" class="uk-switcher">
                    <li><p>Trabajamos para consolidar una red de actores comprometidos con promover una gobernanza abierta, participativa y transparente. Nos orientamos hacia el compromiso con la equidad y la inclusión, guiándonos en la acción pública coherente con las necesidades y aspiraciones de la ciudadanía.</p></li>
                    <li><p> Entregamos herramientas y co-construimos recomendaciones que orientan la toma de decisiones públicas en favor de la participación ciudadana, conectando actores multisectoriales en procesos de diseño, y fortalecemos capacidades en la gestión pública sobre apertura, transparencia e innovación.</p></li>
                  <!--  <li><p>Entregamos herramientas y co-construimos recomendaciones que orientan la toma de decisiones pública en favor de la participción ciudadana, conectando actores multisectoriales en procesos de diseño, y fortalecemos capacidades en la gestión pública sobre apertura, transparencia e innovación</p></li>-->
                </ul>
            </div>
        </div>
    </div>
</div>  
</section>

</div>

<!--<div class="img-grupo">
    <img src="/assets/images/banners/fondo-personas-3.png" alt="img grupo abre alcaldías" class="img" width="100%">
</div> -->

<section id="cta-abre">
    <div class="uk-container uk-container-small uk-padding">
         <div class="uk-width-1 uk-flex uk-flex-center uk-flex-around@s uk-flex-middle uk-flex-wrap">
             <h3 class="animated fadeInUp delay-1s fast uk-text-center uk-text-center@s uk-text-left@m"><span>Únete a nuestra</span> Red de innovadores municipales<br></h3>
             <!--modal -->
             <a href="https://forms.gle/UjPjP3xW9vUDmEaTA" class="animated fadeInUp delay-1s fast btn-abre" target="_blank">Sé parte</a>
         <!-- This is the modal -->
         <!--<div id="modal-example" uk-modal>
             <div class="uk-modal-dialog uk-modal-body">
                 <h2 class="uk-modal-title">Headline</h2>
                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                 <p class="uk-text-right">
                     <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                     <button class="uk-button uk-button-primary" type="button">Save</button>
                 </p>
             </div>
         </div>-->

         </div>
    </div>
</section>


<!--<div class="bg-preguntas">

<section class="uk-container uk-container-small" id="preguntas">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">Preguntas Frecuentes</h3>
        </div>

  <div class="uk-width-1">
        <ul uk-accordion="multiple: true">
            <li class="uk-open">
                <a class="uk-accordion-title" href="#">1. ¿Qué es Abre Alcaldías?</a>
                <div class="uk-accordion-content">
                    <p>Con el objetivo de promover gobiernos locales más abiertos y participativos en América Latina se crea Abre Alcaldías<, una instancia de fortalecimiento de capacidades a personas parte de gobiernos locales, equipos de trabajo de autoridades, integrantes de secretarías o personas con trabajo directo con la comunidad, para la promoción de una gestión participativa, integradora y enfocada en la ciudadanía.</p>
                    <p>Abre Alcaldías es un proyecto de entrega de contenidos, herramientas y generación de red para fortalecer las capacidades de la gestión pública enfocado en la promoción de la innovación, mecanismos de participación ciudadana y representación diversa en el proceso de toma de decisiones.</p>
                </div>
            </li>
            <li>
                <a class="uk-accordion-title" href="#">2. Proceso de solicitud</a>
                <div class="uk-accordion-content">
                    <p>Actualmente Abre Alcaldias no cuenta con procesos abiertos de convocatoria. Sin embargo, puedes inscribirte a nuestra red de innovadores municipales, revisar nuestras herramientas, nuestras redes sociales o escribirnos para saber más de los contenidos o futuros procesos.</p>
                </div>
            </li>
            <li>
                <a class="uk-accordion-title" href="#">3. ¿Qué incluye la participación en Abre Alcaldías?</a>
                <div class="uk-accordion-content">
                    <p>Formación general: Se realizan varios módulos de clases en formato virtual que dan una mirada integral y holística sobre la apertura de gobiernos y los procesos de participación ciudadana en la toma de decisiones. Algunos elementos del currículum son:</p>

                    <ul>
                       <li><p>Desconfianza institucional en Latinoamérica y en nuestros territorios.</p></li>
                       <li><p>Estándares y recomendaciones para una gestión pública participativa, representativa y abierta.</p></li>
                       <li><p>Diseño y evaluación de proyectos con enfoque participativo</p></li>
                       <li><p>Procesos de innovación y metodologías ágiles en la región y el mundo</p></li>
                    </ul>

                    <p><span>Diagnóstico local:</span> Se generan espacios donde los equipos de gobierno local y organizaciones de la sociedad civil evalúan su propio contexto, identificando metas de apertura y participación para su territorio.</p>

                    <p><span>Planes personalizados:</span>  El diagnóstico local realizado permite generar acciones de cambio personalizadas y alcanzables para cada territorio, favoreciendo la participación ciudadana y la co-construcción.</p>

                    <p><span>Acompañamiento e intercambio:</span> Etapa de implementación de los planes personalizados, los cuales se complementan con espacios de encuentro e intercambio con experiencias de otros gobiernos locales y ciudadanía.</p>

                    <p>Si tienes más dudas sobre este punto, puedes contactarnos</p>
                </div>
            </li>

            <li>
                <a class="uk-accordion-title" href="#">4. ¿Quiénes pueden solicitar?</a>
                <div class="uk-accordion-content">
                    <p>Cuando existan procesos de convocatoria abiertos, será importante:</p>
                    <ul>
                       <li><p>Contar con un equipo comprometido con procesos de apertura, participación ciudadana y transparencia, que además trabaje o tenga fuerte conexión con su gobierno local.</p></li>
                       <li><p>Tendrán prioridad aquellos equipos que integren mayor diversidad en términos de raza, género, etnia, entre otras.</p></li>
                       <li><p>Representación de al menos un 40% de mujeres y/o disidencias en los equipos.</p></li>
                       <li><p>Disponibilidad horaria de 4 horas semanales.</p></li>
                       <li><p>Permiso de la máxima autoridad correspondiente.</p></li>
                    </ul>
                </div>
            </li>

            <li>
                <a class="uk-accordion-title" href="#">5. ¿Tengo que pagar para ser parte de Abre Alcaldías?</a>
                <div class="uk-accordion-content">
                    <p>No, la totalidad del proyecto es gratuito.</p>
                </div>
            </li>

            <li>
                <a class="uk-accordion-title" href="#">6. ¿Abre Alcaldías se ha hecho antes?</a>
                <div class="uk-accordion-content">
                    <p>Sí, Abre Alcaldías se realizó el año 2020 y 2021 en México, El Salvador, Guatemala y Ecuador, dando un total de 78 personas formadas. Puedes revisar algunas historias de éxito en nuestra web de Abre Alcaldías</p>
                </div>
            </li>

            <li>
                <a class="uk-accordion-title" href="#">7. ¿Qué es Fundación Ciudadanía Inteligente?</a>
                <div class="uk-accordion-content">
                    <p>Somos una organización sin fines de lucro, latinoamericana que lucha por la justicia social y el fortalecimiento de las democracias. Con 13 años de experiencia en 14 países, investigamos los obstáculos para la participación política de la ciudadanía y aplicamos nuestros hallazgos para promover nuevos marcos institucionales, construir narrativas transformadoras, formar agentes de cambio y articular procesos de fortalecimiento y resiliencia democrática. Conoce más sobre esta organización en: <a href="https://ciudadaniai.org/" target="_blank">https://ciudadaniai.org/</a></p>
                </div>
            </li>

        </ul>
   </div>

    </div>
</section>

</div> -->
