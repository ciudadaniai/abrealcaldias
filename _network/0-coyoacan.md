---
municipality: Coyoacán, CDMX
image: ../assets/images/municipios/prototipoimagen-municipios.png
country: México
description-small: 'Unificar sistemas de atención municipal: presencial y virtual'

modal: coyoacan
destacado: 'Unificar sistemas de atención municipal: presencial y virtual'
descripcion_parrafo_1: 'Durante el 2020 el municipio de Coyoacán participó en Abre Alcaldías para profundizar estrategias en su plan de trabajo municipal, el cual tenía como objetivo crear un sistema unificado de atención ciudadana, que permita que servicios y trátimes digitales y presenciales sean más accesibles, más coherentes y estén mejor conectados.'
descripcion_parrafo_2: 'En una primera fase, han logrado disminuir 124 servicios a 43, y mientras el proyecto avanza, aplican diversas encuestas y levantamiento de información para la ciudadanía con el objetivo de identificar prioridades y urgencias.'
descripcion_parrafo_3:
descripcion_parrafo_4:

implementador_1_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_1: Viridiana Hernández
profesion_implementador_1: Subdirectora de Innovación Participativa
linkedin_implementador_1:
twitter_implementador_1: https://twitter.com/virihg
facebook_implementador_1:
youtube_implementador_1:
website_implementador_1:

implementador_2_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_2: Christian Manterola Cornejo
profesion_implementador_2: Administrador público
linkedin_implementador_2:
twitter_implementador_2:
facebook_implementador_2:
youtube_implementador_2:
website_implementador_2:

implementador_3_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_3: Iris Achá Mendoza
profesion_implementador_3: Contadora Pública
linkedin_implementador_3:
twitter_implementador_3:
facebook_implementador_3:
youtube_implementador_3:
website_implementador_3:

implementador_4_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_4: Jorge Rodríguez Cortes
profesion_implementador_4: Coordinador de Proyectos en Innovación
linkedin_implementador_4:
twitter_implementador_4:
facebook_implementador_4:
youtube_implementador_4:
website_implementador_4:

implementador_5_img:
nombre_implementador_5:
profesion_implementador_5:
linkedin_implementador_5:
twitter_implementador_5:
facebook_implementador_5:
youtube_implementador_5:
website_implementador_5:

implementador_6_img:
nombre_implementador_6:
profesion_implementador_6:
linkedin_implementador_6:
twitter_implementador_6:
facebook_implementador_6:
youtube_implementador_6:
website_implementador_6:

slide_img_1: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_2: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_3: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_4: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_5: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_6: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_7: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_8: ../assets/images/municipios/prototipoimagen-municipios.png

video:

---
