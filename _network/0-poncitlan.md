---
municipality: Poncitlán, Jalisco
image: ../assets/images/municipios/prototipoimagen-municipios.png
country: México
description-small: Problema a resolver o Temática

modal: poncitlan
destacado: 'Transparencia y acceso a la información en obras públicas municipales'
descripcion_parrafo_1: 'Hoy trabaja en torno al ordenamiento territorial y a la construcción de planes de desarrollo urbano en un contexto donde la población crece día a día. Hace 15 años se realizó la última actualización de este plan, que incluyó consultas públicas con varios sectores de la ciudadanía. Sin embargo, estos espacios participativos no tuvieron seguimiento, ni tampoco fueron debidamente comunicados a la comunidad.'
descripcion_parrafo_2: 'A partir de esto, el ayuntamiento de Poncitlán trabaja para impulsar procesos participativos que cumplan con los más altos estándares de cooperación, co-creación y transparencia para la actualización del Plan de Desarrollo Urbano junto a sus vecinas y vecinos, promoviendo nuevas estrategias de ordenamiento territorial de manera participativa.'
descripcion_parrafo_3:
descripcion_parrafo_4:

implementador_1_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_1: David Carrillo Muñoz
profesion_implementador_1: Director de Planeación Urbana Municipal
linkedin_implementador_1:
twitter_implementador_1:
facebook_implementador_1:
youtube_implementador_1:
website_implementador_1:

implementador_2_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_2: Juan Barrón Cinesio
profesion_implementador_2: Técnico en electrónica, actualmente Inspector auxiliar administrativo
linkedin_implementador_2:
twitter_implementador_2:
facebook_implementador_2:
youtube_implementador_2:
website_implementador_2:

implementador_3_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_3: Oscar Mendoza Luna
profesion_implementador_3: Abogado, actualmente Auxiliar
linkedin_implementador_3:
twitter_implementador_3:
facebook_implementador_3:
youtube_implementador_3:
website_implementador_3:

implementador_4_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_4: Reinher Trejo Talavera
profesion_implementador_4: Ingeniero en Computación
linkedin_implementador_4:
twitter_implementador_4:
facebook_implementador_4:
youtube_implementador_4:
website_implementador_4:

implementador_5_img:
nombre_implementador_5:
profesion_implementador_5:
linkedin_implementador_5:
twitter_implementador_5:
facebook_implementador_5:
youtube_implementador_5:
website_implementador_5:

implementador_6_img:
nombre_implementador_6:
profesion_implementador_6:
linkedin_implementador_6:
twitter_implementador_6:
facebook_implementador_6:
youtube_implementador_6:
website_implementador_6:

slide_img_1: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_2: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_3: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_4: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_5: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_6: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_7: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_8: ../assets/images/municipios/prototipoimagen-municipios.png

video:

---
