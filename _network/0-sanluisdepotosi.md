---
municipality: San Luis de Potosí, San Luis de Potosí
image: ../assets/images/municipios/prototipoimagen-municipios.png
country: México
description-small: Tramitaciones digitales más eficientes y transparentes

modal: sanluisdepotosi
destacado: 'Tramitaciones digitales más eficientes y transparentes'
descripcion_parrafo_1: 'El equipo del ayuntamiento de Potosí trabajó durante 2020 en dos iniciativas para mejorar la calidad de vida de sus vecinos y vecinas. '
descripcion_parrafo_2: 'La primera es el "Visor urbano", que ofrece a la ciudadanía acceder a información sobre las obras en construcción, rutas de aseo, consultar licencias de funcionamiento, ubicación de fallas geológicas y permite agilizar las gestiones de licencias de construcción de obras que no excedan los 1.500 mt2 y que se encuentren reguladas en las condicionantes de Uso de Suelo definidas por el municipio, todo a través del espacio virtual.'
descripcion_parrafo_3: 'La segunda es la App "Gobierno Municipal de San Luis" la cual cuenta con 7 módulos de servicios en los que las personas pueden acceder a información sobre el pago predial, atención ciudadana, pago de multas y consultas de servicios de tránsito, entre otras.'
descripcion_parrafo_4:

implementador_1_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_1: Blanca Esmeralda Ramos
profesion_implementador_1: Licenciada en Administración pública
linkedin_implementador_1:
twitter_implementador_1:
facebook_implementador_1:
youtube_implementador_1:
website_implementador_1:

implementador_2_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_2: Javier Isaac López Sánchez
profesion_implementador_2: Director de Innovación Tecnológica
linkedin_implementador_2:
twitter_implementador_2:
facebook_implementador_2: https://www.facebook.com/JavierLopez.slpVI/
youtube_implementador_2:
website_implementador_2:

implementador_3_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_3: José Arellano Hernández
profesion_implementador_3: Ingeniero en Computación
linkedin_implementador_3: https://www.linkedin.com/in/ivanarellanoz/
twitter_implementador_3:
facebook_implementador_3:
youtube_implementador_3:
website_implementador_3:

implementador_4_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_4: Raul Rodriguez Guerrero
profesion_implementador_4: Servidor publico
linkedin_implementador_4:
twitter_implementador_4:
facebook_implementador_4:
youtube_implementador_4:
website_implementador_4:

implementador_5_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_5: Luis Infante
profesion_implementador_5: Área de Transparencia
linkedin_implementador_5:
twitter_implementador_5:
facebook_implementador_5:
youtube_implementador_5:
website_implementador_5:

implementador_6_img:
nombre_implementador_6:
profesion_implementador_6:
linkedin_implementador_6:
twitter_implementador_6:
facebook_implementador_6:
youtube_implementador_6:
website_implementador_6:

slide_img_1:
slide_img_2:
slide_img_3:
slide_img_4:
slide_img_5:
slide_img_6:
slide_img_7:
slide_img_8:

video: https://www.youtube.com/embed/wd1E7m2Lt24

---
