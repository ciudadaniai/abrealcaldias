---
municipality: Benito Juárez, Quintana Roo
image: ../assets/images/municipios/prototipoimagen-municipios.png
country: México
description-small: Servicios básicos y asentamientos informales

modal: benito-juarez
destacado: 'Servicios básicos, calidad de vida, participación ciudadana y asentamientos informales'
descripcion_parrafo_1: 'Durante el 2020 el equipo de Benito Juárez, Quintana Roo quiso enfrentar un grave problema: la existencia de asentamientos irregulares que, al no ser “municipalizados”, no cuentan con los servicios básicos que suele brindar el gobierno local.'
descripcion_parrafo_2: 'Ante este escenario, el equipo se organizó y comenzó el proceso para implementar un presupuesto participativo de manera integral y enfocado especialmente a estos fraccionamientos no municipalizados con un claro objetivo: abordar la falta de servicios de zonas habitacionales sin drenaje y sin iluminación.'
descripcion_parrafo_3: 'Pero aún hay más. El segundo desafío que se propuso el municipio es simplificar los trámites administrativos en torno a la Licencia de Funcionamiento para establecimientos comerciales, industriales o de servicio. Esto mediante un proceso de mejora regulatorio, colaborativo, participativo y con enfoque en la ciudadanía, para construir procesos más eficientes, seguros y económicos.'
descripcion_parrafo_4:

implementador_1_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_1: Carla García
profesion_implementador_1: Activista
linkedin_implementador_1: https://www.linkedin.com/in/carla-garcia-r
twitter_implementador_1: https://twitter.com/carlagardz
facebook_implementador_1:
youtube_implementador_1:
website_implementador_1:

implementador_2_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_2: Carlos Pérez Gutiérrez
profesion_implementador_2: Coordinador de operaciones y logística
linkedin_implementador_2:
twitter_implementador_2:
facebook_implementador_2:
youtube_implementador_2:
website_implementador_2:

implementador_3_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_3: Enrique Encalada
profesion_implementador_3: Director de Área de Planeación
linkedin_implementador_3:
twitter_implementador_3:
facebook_implementador_3:
youtube_implementador_3: https://www.youtube.com/user/eeencalada1
website_implementador_3:

implementador_4_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_4: Perla Aguilar Marfil
profesion_implementador_4: Directora Financiera y Profesional Técnico
linkedin_implementador_4:
twitter_implementador_4:
facebook_implementador_4:
youtube_implementador_4:
website_implementador_4:

implementador_5_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_5: Reyna Valdivia Arceo Rosado
profesion_implementador_5: Contralora Municipal
linkedin_implementador_5:
twitter_implementador_5:
facebook_implementador_5:
youtube_implementador_5:
website_implementador_5:

implementador_6_img:
nombre_implementador_6:
profesion_implementador_6:
linkedin_implementador_6:
twitter_implementador_6:
facebook_implementador_6:
youtube_implementador_6:
website_implementador_6:

slide_img_1:
slide_img_2:
slide_img_3:
slide_img_4:
slide_img_5:
slide_img_6:
slide_img_7:
slide_img_8:

video: https://www.youtube.com/embed/jwTgpuuBxDA


---
