---
municipality: Durango, Durango
image: ../assets/images/municipios/prototipoimagen-municipios.png
country: México
description-small: Transparencia y acceso a la información

modal: durango
destacado: 'Transparencia y acceso a la información en obras públicas municipales'
descripcion_parrafo_1: 'Durante 2020 el municipio de Durango participó en Abre Alcaldías para potenciar su lucha por la transparencia y el correcto uso de los recursos de los pueblos.'
descripcion_parrafo_2: 'Su objetivo final es generar una política de transparencia de datos que permita que la contratación de obras públicas sea más segura y de acceso público, potenciando así una cultura de apertura y de confianza institucional.'
descripcion_parrafo_3: 'Durante su participación en el proeycto el equipo realizó reuniones de acercamiento y sensibilización con autoridades y tomadores de decisión y cabildeos con la comunidad. '
descripcion_parrafo_4:

implementador_1_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_1: Emmanuel Salazar
profesion_implementador_1: Periodista
linkedin_implementador_1:
twitter_implementador_1:
facebook_implementador_1:
youtube_implementador_1:
website_implementador_1:

implementador_2_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_2: María de Lourdes López
profesion_implementador_2: Maestría en comunicación
linkedin_implementador_2:
twitter_implementador_2: https://twitter.com/lululopezsalas
facebook_implementador_2:
youtube_implementador_2:
website_implementador_2:

implementador_3_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_3: Jorge Fabila Flores
profesion_implementador_3: Emprendedor público
linkedin_implementador_3:
twitter_implementador_3:
facebook_implementador_3:
youtube_implementador_3:
website_implementador_3:

implementador_4_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_4: Salvador Perez Alvarez
profesion_implementador_4: Empresario
linkedin_implementador_4:
twitter_implementador_4:
facebook_implementador_4:
youtube_implementador_4:
website_implementador_4:

implementador_5_img:
nombre_implementador_5:
profesion_implementador_5:
linkedin_implementador_5:
twitter_implementador_5:
facebook_implementador_5:
youtube_implementador_5:
website_implementador_5:

implementador_6_img:
nombre_implementador_6:
profesion_implementador_6:
linkedin_implementador_6:
twitter_implementador_6:
facebook_implementador_6:
youtube_implementador_6:
website_implementador_6:

slide_img_1: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_2: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_3: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_4: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_5: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_6: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_7: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_8: ../assets/images/municipios/prototipoimagen-municipios.png

video:

---
