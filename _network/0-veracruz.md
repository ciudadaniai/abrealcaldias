---
municipality: Veracruz, Veracruz
image: ../assets/images/municipios/prototipoimagen-municipios.png
country: México
description-small: Atención municipal, innovación y sustentabilidad para la ciudadanía

modal: veracruz
destacado: 'Administración municipal con enfoque en la ciudadanía: Atención, innovación y sustentabilidad'
descripcion_parrafo_1: 'Durante el 2020 el municipio de Veracruz participó en Abre Alcaldías para profundizar en su eje institucional “Te Quiero en Marcha” que buscó “eficientar los servicios de la administración municipal priorizando la atención ciudadana; simplificando los trámites y servicios con mecanismos de innovación y sustentabilidad; realizando reducciones en el gasto, y transparentado el uso de los recursos públicos como un gobierno abierto y cercano a su ciudadanía”'
descripcion_parrafo_2: 'Para esto, se generaron acciones que buscaran facilitar el acceso y la difusión de información pública a la par de la implementación de mecanismos de participación ciudadana para recepción de opiniones y propuestas.'
descripcion_parrafo_3:
descripcion_parrafo_4:

implementador_1_img:
nombre_implementador_1:
profesion_implementador_1:
linkedin_implementador_1:
twitter_implementador_1:
facebook_implementador_1:
youtube_implementador_1:
website_implementador_1:

implementador_2_img:
nombre_implementador_2:
profesion_implementador_2:
linkedin_implementador_2:
twitter_implementador_2:
facebook_implementador_2:
youtube_implementador_2:
website_implementador_2:

implementador_3_img:
nombre_implementador_3:
profesion_implementador_3:
linkedin_implementador_3:
twitter_implementador_3:
facebook_implementador_3:
youtube_implementador_3:
website_implementador_3:

implementador_4_img:
nombre_implementador_4:
profesion_implementador_4:
linkedin_implementador_4:
twitter_implementador_4:
facebook_implementador_4:
youtube_implementador_4:
website_implementador_4:

implementador_5_img:
nombre_implementador_5:
profesion_implementador_5:
linkedin_implementador_5:
twitter_implementador_5:
facebook_implementador_5:
youtube_implementador_5:
website_implementador_5:

implementador_6_img:
nombre_implementador_6:
profesion_implementador_6:
linkedin_implementador_6:
twitter_implementador_6:
facebook_implementador_6:
youtube_implementador_6:
website_implementador_6:

slide_img_1:
slide_img_2:
slide_img_3:
slide_img_4:
slide_img_5:
slide_img_6:
slide_img_7:
slide_img_8:

video: https://www.youtube.com/embed/b-jh1o3lOAs

---
