---
municipality: Iztapalapa, CDMX
image: ../assets/images/municipios/prototipoimagen-municipios.png
country: México
description-small: Problema a resolver o Temática

modal: iztapalapa
destacado: 'Problema a resolver o Temática'
descripcion_parrafo_1: 'Pero el municipio no se queda en el pasado y hoy trabaja para corregir la falta de participación de la comunidad en los procesos políticos locales. A lo largo de los años, la falta de espacios de interacción y decisión sobre los propios barrios ha llevado a la ruptura progresiva de la convivencia y la cohesión social, donde las personas no se sienten parte de su comunidad y así aumentó la violencia, la delincuencia y el extremo individualismo.'
descripcion_parrafo_2: 'En este escenario, el municipio de Iztapalapa trabaja en el proyecto “Planeando y transformando Iztapalapa”. Una iniciativa que consiste en la promoción de procesos de planeación participativa, donde barrio por barrio se fortalecerán las capacidades de las comunidades para dirigir su comunidad, se conformarán colectivos de coordinación multisectoriales y la construcción de agendas de desarrollo local. Así, se espera potenciar ciudadanías y sociedades activas, empoderadas, participativas, capaces de exigir sus derechos, bajo un esquema de gobernanza democrática, participativa, solidaria y pacífica.'
descripcion_parrafo_3:
descripcion_parrafo_4:

implementador_1_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_1: Alicia Herrera Martínez
profesion_implementador_1: Subdirectora de ventanilla única de trámites
linkedin_implementador_1:
twitter_implementador_1:
facebook_implementador_1:
youtube_implementador_1:
website_implementador_1:

implementador_2_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_2: Maria de las Mercedes Camargo
profesion_implementador_2: Líder Coordinador de Proyectos
linkedin_implementador_2:
twitter_implementador_2:
facebook_implementador_2:
youtube_implementador_2:
website_implementador_2:

implementador_3_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_3: Rocío Lombera
profesion_implementador_3: Directora General de Planeación y Participación Ciudadana Encalada
linkedin_implementador_3:
twitter_implementador_3:
facebook_implementador_3:
youtube_implementador_3:
website_implementador_3:

implementador_4_img:
nombre_implementador_4:
profesion_implementador_4:
linkedin_implementador_4:
twitter_implementador_4:
facebook_implementador_4:
youtube_implementador_4:
website_implementador_4:

implementador_5_img:
nombre_implementador_5:
profesion_implementador_5:
linkedin_implementador_5:
twitter_implementador_5:
facebook_implementador_5:
youtube_implementador_5:
website_implementador_5:

implementador_6_img:
nombre_implementador_6:
profesion_implementador_6:
linkedin_implementador_6:
twitter_implementador_6:
facebook_implementador_6:
youtube_implementador_6:
website_implementador_6:

slide_img_1: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_2: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_3: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_4: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_5: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_6: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_7: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_8: ../assets/images/municipios/prototipoimagen-municipios.png

video:

---
