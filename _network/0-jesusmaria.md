---
municipality: Jesús María, Aguascalientes
image: ../assets/images/municipios/prototipoimagen-municipios.png
country: México
description-small: Participación ciudadana en tiempos de cuarentena

modal: jesusmaria
destacado: 'Participación ciudadana en tiempos de cuarentena'
descripcion_parrafo_1: 'Durante el 2020 y fuertemente influenciado por el contexto sanitario, el Ayuntamiento Jesús María promovió el uso de  las redes sociales para acercar la ciudadanía a la gestión municipal.'
descripcion_parrafo_2: 'Con este objetivo y a través de Facebook y otras plataformas similares crearon la iniciativa “Miércoles Ciudadano”. Un proyecto que busca impulsar la innovación sin modificar el gasto público, generando instancias coordinadas donde los ciudadanos y ciudadanas planteen sus dudas, incluso en vivo, y que éstas puedan ser respondidas en directo por los tomadores de decisión.'
descripcion_parrafo_3:
descripcion_parrafo_4:

implementador_1_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_1: Jesús Medina Olivares
profesion_implementador_1: Politólogo
linkedin_implementador_1:
twitter_implementador_1: https://twitter.com/jesusmedinao
facebook_implementador_1:
youtube_implementador_1:
website_implementador_1:

implementador_2_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_2: Alejandra Gonzalez
profesion_implementador_2: Maestra en Administración, Integrante del Comité de Participación ciudadana
linkedin_implementador_2:
twitter_implementador_2:
facebook_implementador_2:
youtube_implementador_2:
website_implementador_2:

implementador_3_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_3: Elsa Guzmán Martínez
profesion_implementador_3: Asistente Administrativo Comité de Participación Ciudadana
linkedin_implementador_3:
twitter_implementador_3:
facebook_implementador_3:
youtube_implementador_3:
website_implementador_3:

implementador_4_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_4: Francisco Aguirre Arias
profesion_implementador_4: Comisionado Ciudadano del Sistema Anticorrupción
linkedin_implementador_4:
twitter_implementador_4:
facebook_implementador_4:
youtube_implementador_4:
website_implementador_4:

implementador_5_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_5: José de Jesús Suárez Mariscal
profesion_implementador_5: Comisionado del Comité de Participación Ciudadana
linkedin_implementador_5:
twitter_implementador_5:
facebook_implementador_5:
youtube_implementador_5:
website_implementador_5:

implementador_6_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_6: Josefina Diaz Aguilar
profesion_implementador_6: Integrante del Comité de Participación Ciudadana
linkedin_implementador_6:
twitter_implementador_6:
facebook_implementador_6:
youtube_implementador_6:
website_implementador_6:

implementador_7_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_7: Netzahualcóyotl López
profesion_implementador_7: Comisionado Presidente del Comité de Participación
linkedin_implementador_7:
twitter_implementador_7:
facebook_implementador_7:
youtube_implementador_7:
website_implementador_7:

slide_img_1: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_2: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_3: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_4: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_5: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_6: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_7: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_8: ../assets/images/municipios/prototipoimagen-municipios.png

video:

---
