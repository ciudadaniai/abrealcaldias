---
municipality: León, Guanajuato
image: ../assets/images/municipios/prototipoimagen-municipios.png
country: México
description-small: Construcción participativa de la comunidad y sus áreas verdes

modal: guanajuato
destacado: 'Construcción participativa de la comunidad y sus áreas verdes'
descripcion_parrafo_1: 'El equipo de LAB León, en conjunto con el gobierno local, avanzaron durante el 2020 en proyectos de co-diseño de espacios públicos de manera participativa y vinculante.'
descripcion_parrafo_2: 'Con el objetivo de lograr que todas las personas que habitan en el ayuntamiento se sientan parte del espacio y se hagan responsable de su cuidado, logrando así un uso más eficiente de los recursos.'
descripcion_parrafo_3: 'La certeza es clara: si trabajamos juntos y juntas por crear, implementar y cuidar los espacios, haremos mejores municipios.'
descripcion_parrafo_4:

implementador_1_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_1: Carlos Torres Barrientos
profesion_implementador_1: Director LAB León
linkedin_implementador_1:
twitter_implementador_1:
facebook_implementador_1:
youtube_implementador_1:
website_implementador_1:

implementador_2_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_2: Ricardo Mirón
profesion_implementador_2: Hacker cívico, diseñador y desarrollador de software
linkedin_implementador_2:
twitter_implementador_2:
facebook_implementador_2:
youtube_implementador_2:
website_implementador_2:

implementador_3_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_3: Claudia del Carmen García
profesion_implementador_3: Encargada de Coordinación Oficios Digitales
linkedin_implementador_3:
twitter_implementador_3:
facebook_implementador_3:
youtube_implementador_3:
website_implementador_3:

implementador_4_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_4: Cristian González Muñoz
profesion_implementador_4: Abogado y Coordinador Administrativo
linkedin_implementador_4:
twitter_implementador_4:
facebook_implementador_4:
youtube_implementador_4:
website_implementador_4:

implementador_5_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_5: Karen Lizzeth Du Pont Falcón
profesion_implementador_5: Trabajadora de administración local
linkedin_implementador_5:
twitter_implementador_5:
facebook_implementador_5:
youtube_implementador_5:
website_implementador_5:

implementador_6_img:
nombre_implementador_6:
profesion_implementador_6:
linkedin_implementador_6:
twitter_implementador_6:
facebook_implementador_6:
youtube_implementador_6:
website_implementador_6:

slide_img_1: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_2: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_3: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_4: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_5: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_6: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_7: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_8: ../assets/images/municipios/prototipoimagen-municipios.png

video:

---
