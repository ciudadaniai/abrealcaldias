---
municipality: Municipio Ecuador
image: ../assets/images/municipios/prototipoimagen-municipios.png
country: Ecuador
description-small: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a suscipit metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.

modal: ejemplo-ecuador
destacado: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a suscipit metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.'
descripcion_parrafo_1: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a suscipit metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.'
descripcion_parrafo_2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a suscipit metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.'
descripcion_parrafo_3: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a suscipit metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.'
descripcion_parrafo_4: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a suscipit metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.'


implementador_1_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_1:
profesion_implementador_1:
linkedin_implementador_1:
twitter_implementador_1:
facebook_implementador_1:
youtube_implementador_1:
website_implementador_1:

implementador_2_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_2:
profesion_implementador_2:
linkedin_implementador_2:
twitter_implementador_2:
facebook_implementador_2:
youtube_implementador_2:
website_implementador_2:

implementador_3_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_3:
profesion_implementador_3:
linkedin_implementador_3:
twitter_implementador_3:
facebook_implementador_3:
youtube_implementador_3:
website_implementador_3:

implementador_4_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_4:
profesion_implementador_4:
linkedin_implementador_4:
twitter_implementador_4:
facebook_implementador_4:
youtube_implementador_4:
website_implementador_4:

implementador_5_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_5:
profesion_implementador_5:
linkedin_implementador_5:
twitter_implementador_5:
facebook_implementador_5:
youtube_implementador_5:
website_implementador_5:

implementador_6_img: ../assets/images/municipios/prototipoimagen-municipios.png
nombre_implementador_6:
profesion_implementador_6:
linkedin_implementador_6:
twitter_implementador_6:
facebook_implementador_6:
youtube_implementador_6:
website_implementador_6:

slide_img_1: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_2: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_3: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_4: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_5: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_6: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_7: ../assets/images/municipios/prototipoimagen-municipios.png
slide_img_8: ../assets/images/municipios/prototipoimagen-municipios.png

video:

---
