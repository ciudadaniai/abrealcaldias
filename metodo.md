---
layout: page-metodo
sub-title:
title: Nuestro Método
permalink: /metodo/
welcome_desktop: ../assets/images/banners/banner-proceso-de-formacion.png
welcome_mobile: ../assets/images/banners/banner-proceso-de-formacion.png
---

<div class="bg-proceso-formacion">
<section class="uk-container uk-container-small uk-margin" id="proceso-formacion">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">

        <div class="paso">
          <div class="titulo">
             <h3 class="animated fadeInUp delay-1s fast uk-text-center">1</h3>
             <h4 class="animated fadeInUp delay-1s fast uk-text-center">Convocatoria y selección</h4>
          </div>
          <div class="descripcion animated fadeInUp delay-1s fast">
             <p>Abre Alcaldías parte con una convocatoria abierta en donde el equipo de Ciudadanía Inteligente invita a funcionarios/as de gobiernos locales a ser parte del proyecto. Luego de eso, se seleccionan aquellos equipos con mayor compromiso con la apertura, la participación ciudadana y la diversidad.</p>
          </div>
          </div>

          <div class="paso">
            <div class="titulo">
               <h3 class="animated fadeInUp delay-1s fast uk-text-center">2</h3>
               <h4 class="animated fadeInUp delay-1s fast uk-text-center">Formación general</h4>
            </div>
            <div class="descripcion animated fadeInUp delay-1s fast">
               <p>Se realizan varios módulos de clases en formato virtual que dan una mirada integral y holística sobre la apertura de gobiernos y los procesos de participación ciudadana en la toma de decisiones. Algunos elementos del currículum son:</p>
               <ul>
                   <li><p>1. Desconfianza institucional en Latinoamérica y en nuestros territorios.</p></li>
                   <li><p>2. Estándares y recomendaciones para una gestión pública participativa, representativa y abierta.</p></li>
                   <li><p>3. Diseño y evaluación de proyectos con enfoque participativo</p></li>
                   <li><p>4. Procesos de innovación y metodologías ágiles en la región y el mundo</p></li>
               </ul>
            </div>
            </div>

            <div class="paso">
              <div class="titulo">
                 <h3 class="animated fadeInUp delay-1s fast uk-text-center">3</h3>
                 <h4 class="animated fadeInUp delay-1s fast uk-text-center">Diagnóstico local</h4>
              </div>
              <div class="descripcion animated fadeInUp delay-1s fast">
                 <p>Se generan espacios de trabajo en donde los equipos de los gobiernos locales y organizaciones de la sociedad civil evalúan su propio contexto, identificando metas de apertura y participación para su territorio.</p>
              </div>
              </div>

              <div class="paso">
                <div class="titulo">
                   <h3 class="animated fadeInUp delay-1s fast uk-text-center">4</h3>
                   <h4 class="animated fadeInUp delay-1s fast uk-text-center">Planes personalizados</h4>
                </div>
                <div class="descripcion animated fadeInUp delay-1s fast">
                   <p>Tras el diagnóstico local realizado, se ofrecen espacios de co-diseño para generar acciones de cambio personalizadas y alcanzables para cada territorio, favoreciendo la participación ciudadana y la co-construcción.</p>
                </div>
                </div>

                <div class="paso">
                  <div class="titulo">
                     <h3 class="animated fadeInUp delay-1s fast uk-text-center">5</h3>
                     <h4 class="animated fadeInUp delay-1s fast uk-text-center">Acompañamiento e intercambio</h4>
                  </div>
                  <div class="descripcion animated fadeInUp delay-1s fast">
                     <p>Acompañamos en la implementación de los planes personalizados, los cuales se complementan con espacios de encuentro e intercambio con experiencias de otros gobiernos locales y ciudadanía.</p>
                  </div>
                  </div>


        </div>
    </div>
</section>
</div>
