---
layout: page-abrealcaldias-chile
sub-title: ¿Qué le falta a los municipios de Chile? La respuesta está en las personas
title: AbreAlcaldías<Chile
permalink: /abrealcaldias-chile/
welcome_desktop: ../assets/images/banners/header-abre-chile.png
welcome_mobile: ../assets/images/banners/header-abre-chile.png
---


<div class="bg-que-es que-es-abrechile">

<section class="uk-container uk-container-medium uk-margin" id="que-es">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">

            <p class="que-es-descripcion animated fadeInUp delay-1s fast uk-text-center">Abre Alcaldías es un <span>programa de formación</span> para avanzar en</p>
            <p class="que-es-descripcion-2 animated fadeInUp delay-1s fast uk-text-center"><span>transparencia, innovación y participación</span> en los ejes de</p>

            <div class="img-linea">
                <img src="/assets/images/banners/mapa-abre-chile.svg" alt="img" class="img animated fadeInUp delay-1s fast" width="100%">
            </div>

            <p class="que-es-descripcion-3 animated fadeInUp delay-1s fast uk-text-center">Dirigido a <span>funcionarios y funcionarias</span></p>
            <p class="que-es-descripcion-4 animated fadeInUp delay-1s fast uk-text-center"><span>municipales</span> de todo Chile</p>

             <!--<div class="btn-inscripcion">
                  <a href="https://forms.gle/WS4iHrtmuRbh7Rb39" class="btn-important animated fadeInUp delay-1s fast uk-text-center" target="_blank">Inscripciones aquí</a>
             </div>-->

        </div>
    </div>
</section>
</div>



<div class="bg-preguntas bg-preguntas-abrechile">
<section class="uk-container uk-container-small" id="preguntas">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">Sobre el programa</h3>
        </div>

  <div class="uk-width-1">
        <ul uk-accordion="multiple: true">
            <li class="uk-open">
                <a class="uk-accordion-title" href="#">1. ¿Qué es AbreAlcaldías&lt;Chile?</a>
                <div class="uk-accordion-content">
                    <p>AbreAlcaldías&lt;Chile es un programa de formación en políticas participativas en los ejes de medio ambiente, interculturalidad y/o equidad de género, dirigido a funcionarios y funcionarias municipales de todo Chile. </p>
                    <p>Es un proyecto que desde 2018 está trabajando junto a la gestión pública local de más de 40 municipios latinoamericanos para impulsar metodologías y herramientas que potencian políticas públicas más innovadoras, transparentes y representativas</p>
                </div>
            </li>
          <!--  <li>
                <a class="uk-accordion-title" href="#">2. ¿Hasta cuándo se puede postular?</a>
                <div class="uk-accordion-content">
                    <p>La convocatoria para la primera etapa de talleres virtuales de AbreAlcaldías&lt;Chile estará abierta desde el 16 de mayo hasta el 4 de junio de 2023.</p>
                </div>
            </li> -->
            <li>
                <a class="uk-accordion-title" href="#">2. ¿Qué implica participar?</a>
                <div class="uk-accordion-content">
                    <p>AbreAlcaldías&lt;Chile comienza con una primera etapa de talleres virtuales de entre dos y tres horas, una vez por semana, que se llevarán a cabo durante junio y julio de 2023. Una vez terminados los talleres virtuales, los equipos podrán postular a una segunda etapa de acompañamiento para la implementación de un plan de acción de política pública participativa y recibirán la certificación AbreAlcaldías.</p>
                </div>
            </li>

            <li>
                <a class="uk-accordion-title" href="#">3. ¿Tiene algún costo?</a>
                <div class="uk-accordion-content">
                    <p>No, la totalidad del programa es gratuito.</p>
                </div>
            </li>

            <li>
                <a class="uk-accordion-title" href="#">4. ¿Quiénes pueden postular?</a>
                <div class="uk-accordion-content">
                    <p>Autoridades, equipos o funcionarios/as municipales de cualquier parte de Chile que:</p>
                    <ul>
                       <li><p>Están trabajando o están por iniciar proyectos relacionados a medio ambiente, interculturalidad y/o equidad de género.</p></li>
                       <li><p>Cuentan con un equipo comprometido con procesos de apertura, participación ciudadana y transparencia, que además trabaje o tenga fuerte conexión con su gobierno local.</p></li>
                       <li><p>Cuentan con una representación de al menos un 40% de mujeres y/o disidencias en los equipos.</p></li>
                       <li><p>Cuentan con una disponibilidad horaria de 4 horas semanales por junio y julio.</p></li>
                       <li><p>Cuentan con el permiso de la máxima autoridad correspondiente.</p></li>
                       <li><p>Tendrán prioridad aquellos equipos que integren mayor diversidad en términos de raza, género, etnia, entre otras.</p></li>
                    </ul>
                </div>
            </li>

            <li>
                <a class="uk-accordion-title" href="#">5. Organizaciones aliadas</a>
                <div class="uk-accordion-content">
                    <p>Para cada uno de los ejes temáticos de AbreAlcaldías&lt;Chile se establecieron alianzas de colaboración:</p>
                    <ul>
                       <li><p><span>1. Eje medio ambiental < ONG FIMA:</span> ONG, sin fines de lucro, fundada en 1998, que busca promover activamente el derecho a vivir en un medio ambiente sano y velar por la protección de la naturaleza. ONG FIMA trabaja para ser un aporte significativo a la política, la legislación y el acceso a la justicia ambiental en Chile.</p></li>
                       <li><p><span>2. Eje intercultural < Rizoma Intercultural:</span> Organización sin fines de lucro que promueve los derechos y la inclusión de las personas que migran a Chile a través de la intervención social, la formación, la investigación y el trabajo en red con otras organizaciones migrantes y pro migrantes, desde un enfoque de derechos e intercultural crítico.</p></li>
                       <li><p><span>3. Eje de equidad de género < ONG Amaranta:</span> Organización feminista fundada en Concepción el año 2018. Sus líneas de trabajo son género, derechos humanos, diversidad funcional, tecnologías, niñez y trabajo. Actualmente se encuentran en Santiago, Chillán y Concepción. Con tres grandes proyectos a cargo en la actualidad: Numun, Aurora y Lilén, han podido llegar a una serie de territorios e instituciones aportando desde la educación popular con perspectiva de género.</p></li>
                    </ul>
                </div>
            </li>
        </ul>
   </div>

    </div>
</section>

</div>



<div class="contacto-correo">

<section class="uk-container uk-container-small uk-margin" id="que-es">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <p class="que-es-descripcion animated fadeInUp delay-1s fast uk-text-center">¿Tienes alguna pregunta?</p>
            <p class="que-es-descripcion-2 animated fadeInUp delay-1s fast uk-text-center">Contáctanos en abrealcaldias@ciudadaniai.org</p>
        </div>
    </div>
</section>
</div>

<div class="img-grupo">
    <img src="/assets/images/banners/bg-abre-chile.jpg" alt="img" class="img" width="100%">
</div>
