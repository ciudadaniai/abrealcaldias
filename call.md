---
layout: page
title: Convocatoria
permalink: /call/
welcome_desktop: ../assets/images/call-desktop.jpg
welcome_mobile: ../assets/images/call-mobile.jpg
---

<div class="uk-section section-no-color animated fadeIn delay-1s fast">
  <div class="uk-container uk-width-2-3@m uk-text-center " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h1 class="font-color-2nd-0" style="margin-bottom: -10px;">{% t call.pretitle %}</h1>
        <h2 class="font-color-1st-0" style="margin-bottom: -40px;">{% t call.title_1 %}</h2>
        <h2 class="font-color-1st-0">{% t call.title_2 %}</h2>
        <p>{% t call.description %}</p>
        <a href="mailto:abrealcaldias@ciudadaniai.org" class="uk-button button-color-1st" target="_blank">{% t call.action_0 %}</a>
        <a href="#call" class="uk-hidden uk-button button-color-1st" uk-scroll>{% t call.action_1 %}</a>
        <a href="#application" class="uk-hidden uk-button button-color-2nd" uk-scroll>{% t call.action_2 %}</a>
      </div>
    </div>
  </div>
</div>

<div id="call" class="uk-section section-color-1st animated fadeIn">
  <div class="uk-container uk-width-3-4@m" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
      <div class="uk-width-1-1 uk-flex" uk-grid>
        <div class="uk-width-1-4@m">
          <i class='uil uil-users-alt font-color-1st-0' style="font-size: 50px"></i>
          <h3 style="margin-top:-30px;">{% t call.group_1.title %}</h3>
        </div>
        <div class="uk-width-3-4@m">
          <p class="list-angle font-compressed">{% t call.group_1.description_1 %}</p>
          <p class="list-angle font-compressed">{% t call.group_1.description_2 %}</p>
          <p class="list-angle font-compressed">{% t call.group_1.description_3 %}</p>
        </div>
      </div>
  </div>
</div>

<div class="uk-section section-color-2nd animated fadeIn">
  <div class="uk-container uk-width-3-4@m" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
      <div class="uk-width-1-1 uk-flex" uk-grid>
        <div class="uk-width-1-4@m">
          <i class='uil uil-file-alt font-color-2nd-0' style="font-size: 50px"></i>
          <h3 style="margin-top:-22px;">{% t call.group_2.title %}</h3>
        </div>
        <div class="uk-width-3-4@m">
          <p class="list-angle font-compressed">{% t call.group_2.description_1 %}</p>
          <p class="list-angle font-compressed">{% t call.group_2.description_2 %}</p>
          <p class="list-angle font-compressed">{% t call.group_2.description_3 %}</p>
        </div>
      </div>
  </div>
</div>

<div class="uk-section section-no-color animated fadeIn">
  <div class="uk-container uk-width-3-4@m" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
      <div class="uk-width-1-1 uk-flex" uk-grid>
        <div class="uk-width-1-4@m">
          <i class='uil uil-calendar-alt' style="font-size: 50px"></i>
          <h3 style="margin-top:-22px;">{% t call.group_3.title %}</h3>
        </div>
        <div class="uk-width-3-4@m">
        <dl class="uk-description-list uk-description-list-divider">
          <dt class="font-color-1st-0">{% t call.group_3.subtitle_1 %}</dt>
          <dd class="font-compressed">{% t call.group_3.description_1 %}</dd>
          <dt class="font-color-1st-0">{% t call.group_3.subtitle_2 %}</dt>
          <dd class="font-compressed">{% t call.group_3.description_2 %}</dd>
          <dt class="font-color-1st-0">{% t call.group_3.subtitle_3 %}</dt>
          <dd class="font-compressed">{% t call.group_3.description_3 %}</dd>
          <dt class="font-color-1st-0">{% t call.group_3.subtitle_4 %}</dt>
          <dd class="font-compressed">{% t call.group_3.description_4 %}</dd>
          <dt class="font-color-1st-0">{% t call.group_3.subtitle_5 %}</dt>
          <dd class="font-compressed">{% t call.group_3.description_5 %}</dd>
        </dl>
        <p class="list-angle font-compressed">{% t call.group_3.subtitle_6 %}
        {% t call.group_3.description_6 %}</p>
        <p class="list-angle font-compressed">{% t call.group_3.subtitle_7 %}</p>
        <p class="list-angle font-compressed">{% t call.group_3.subtitle_8 %}
        {% t call.group_3.description_8 %}</p>        
        </div>
      </div>
  </div>
</div>

<div id="application" class="uk-section section-gradient animated fadeIn">
  <div class="uk-container uk-width-1-2@m uk-text-center" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>{% t call.application.title %}</h2>
        <p>{% t call.application.description_1 %} <a href="{{ site.baseurl_root }}/faq">{% t call.application.description_2 %}</a></p>
      </div>
    </div>
  </div>
</div>
