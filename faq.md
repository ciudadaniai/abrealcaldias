---
layout: page
title: Preguntas frecuentes
permalink: /faq/
welcome_desktop: ../assets/images/faq-desktop.jpg
welcome_mobile: ../assets/images/faq-mobile.jpg
---

<div class="uk-section section-no-color animated fadeIn uk-hidden">
  <div class="uk-container uk-width-1-1" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-child-width-1-1 uk-child-width-1-4@m uk-grid-small uk-flex uk-flex-center uk-flex-middle uk-text-center" uk-grid="masonry: true">    
          <div>
            <a href="#"><div class="uk-card uk-card-small uk-card-default uk-card-body section-color-1st">
              <h5>{% t faq.group_1.title %}</h5>
            </div></a>        
          </div>  
          <div>
            <a href="#"><div class="uk-card uk-card-small uk-card-default uk-card-body section-color-2nd">
              <h5>{% t faq.group_2.title %}</h5>
            </div></a>        
          </div>
          <div>
            <a href="#"><div class="uk-card uk-card-small uk-card-default uk-card-body section-color-1st">
              <h5>{% t faq.group_3.title %}</h5>
            </div></a>       
          </div>
          <div>
            <a href="#"><div class="uk-card uk-card-small uk-card-default uk-card-body section-color-2nd">
              <h5>{% t faq.group_4.title %}</h5>
            </div></a>     
          </div>    
      </div>                                            
  </div>
</div>



<div class="uk-section section-color-1st animated fadeIn delay-1s fast">
  <div class="uk-container uk-width-3-4@m" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
      <div class="uk-width-1-1 uk-flex" uk-grid>
        <div class="uk-width-1-4@m">
          <h3 class="no-project-color">{% t faq.group_1.title %}</h3>
        </div>
        <div class="uk-width-3-4@m">
          <ul uk-accordion>
              <li class="uk-open">
                  <a class="uk-accordion-title" href="#">{% t faq.group_1.1.title %}</a>
                  <div class="uk-accordion-content">
                    <p class="list-angle font-compressed">{% t faq.group_1.1.description_1 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_1.1.description_2 %}</p>
                  </div>
              </li>
              <li>
                  <a class="uk-accordion-title" href="#">{% t faq.group_1.2.title %}</a>
                  <div class="uk-accordion-content">
                    <p class="list-angle font-compressed">{% t faq.group_1.2.description_1 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_1.2.description_2 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_1.2.description_3 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_1.2.description_4 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_1.2.description_5 %}</p>
                  </div>
              </li>
              <li>
                  <a class="uk-accordion-title" href="#">{% t faq.group_1.3.title %}</a>
                  <div class="uk-accordion-content">
                    <p class="list-angle font-compressed">{% t faq.group_1.3.description_1 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_1.3.description_2 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_1.3.description_3 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_1.3.description_4 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_1.3.description_5 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_1.3.description_6 %}</p>
                  </div>
              </li>
              <li>
                  <a class="uk-accordion-title" href="#">{% t faq.group_1.4.title %}</a>
                  <div class="uk-accordion-content">
                    <p class="list-angle font-compressed">{% t faq.group_1.4.description_1 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_1.4.description_2 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_1.4.description_3 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_1.4.description_4 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_1.4.description_5 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_1.4.description_6 %}</p>
                  </div>
              </li>
              <li>
                  <a class="uk-accordion-title" href="#">{% t faq.group_1.5.title %}</a>
                  <div class="uk-accordion-content">
                    <p class="list-angle font-compressed">{% t faq.group_1.5.description %}</p>
                  </div>
              </li>
              <li>
                  <a class="uk-accordion-title" href="#">{% t faq.group_1.6.title %}</a>
                  <div class="uk-accordion-content">
                    <p class="list-angle font-compressed">{% t faq.group_1.6.description_1 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_1.6.description_2 %}</p>
                  </div>
              </li>
              <li>
                  <a class="uk-accordion-title" href="#">{% t faq.group_1.7.title %}</a>
                  <div class="uk-accordion-content">
                    <p class="list-angle font-compressed">{% t faq.group_1.7.description %}</p>
                  </div>
              </li>
              <li>
                  <a class="uk-accordion-title" href="#">{% t faq.group_1.8.title %}</a>
                  <div class="uk-accordion-content">
                    <p class="list-angle font-compressed">{% t faq.group_1.8.description %}</p>
                  </div>
              </li>
              <li>
                  <a class="uk-accordion-title" href="#">{% t faq.group_1.9.title %}</a>
                  <div class="uk-accordion-content">
                    <p class="list-angle font-compressed">{% t faq.group_1.9.description %}</p>
                  </div>
              </li>
              <li>
                  <a class="uk-accordion-title" href="#">{% t faq.group_1.10.title %}</a>
                  <div class="uk-accordion-content">
                    <p class="list-angle font-compressed">{% t faq.group_1.10.description %}</p>
                  </div>
              </li>
              <li>
                  <a class="uk-accordion-title" href="#">{% t faq.group_1.11.title %}</a>
                  <div class="uk-accordion-content">
                    <p class="list-angle font-compressed">{% t faq.group_1.11.description %}</p>
                  </div>
              </li>                                                        
          </ul>
        </div>
      </div>
    </div>
  </div>


<div class="uk-section section-color-2nd animated fadeIn">
  <div class="uk-container uk-width-3-4@m" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
      <div class="uk-width-1-1 uk-flex" uk-grid>
        <div class="uk-width-1-4@m">
          <h3 class="no-project-color">{% t faq.group_2.title %}</h3>
        </div>
        <div class="uk-width-3-4@m">
          <ul uk-accordion>
              <li>
                  <a class="uk-accordion-title" href="#">{% t faq.group_2.12.title %}</a>
                  <div class="uk-accordion-content">
                    <p class="list-angle font-compressed">{% t faq.group_2.12.description %}</p>
                  </div>
              </li>
              <li>
                  <a class="uk-accordion-title" href="#">{% t faq.group_2.13.title %}</a>
                  <div class="uk-accordion-content">
                    <p class="list-angle font-compressed">{% t faq.group_2.13.description_1 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_2.13.description_2 %}</p>
                    <p class="list-angle font-compressed">{% t faq.group_2.13.description_3 %}</p>
                  </div>
              </li>
              <li>
                  <a class="uk-accordion-title" href="#">{% t faq.group_2.14.title %}</a>
                  <div class="uk-accordion-content">
                    <p class="list-angle font-compressed">{% t faq.group_2.14.description %}</p>
                  </div>
              </li>
              <li>
                  <a class="uk-accordion-title" href="#">{% t faq.group_2.15.title %}</a>
                  <div class="uk-accordion-content">
                    <p class="list-angle font-compressed">{% t faq.group_2.15.description %}</p>
                  </div>
              </li>
              <li>
                  <a class="uk-accordion-title" href="#">{% t faq.group_2.16.title %}</a>
                  <div class="uk-accordion-content">
                    <p class="list-angle font-compressed">{% t faq.group_2.16.description %}</p>
                  </div>
              </li>
          </ul>
        </div>
      </div>
    </div>
  </div>


  <div class="uk-section section-color-1st animated fadeIn">
    <div class="uk-container uk-width-3-4@m" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
        <div class="uk-width-1-1 uk-flex" uk-grid>
          <div class="uk-width-1-4@m">
            <h3 class="no-project-color">{% t faq.group_3.title %}</h3>
          </div>
          <div class="uk-width-3-4@m">
            <ul uk-accordion>
                <li>
                    <a class="uk-accordion-title" href="#">{% t faq.group_3.17.title %}</a>
                    <div class="uk-accordion-content">
                      <p class="list-angle font-compressed">{% t faq.group_3.17.description %}</p>
                    </div>
                </li>
                <li>
                    <a class="uk-accordion-title" href="#">{% t faq.group_3.18.title %}</a>
                    <div class="uk-accordion-content">
                      <p class="list-angle font-compressed">{% t faq.group_3.18.description %}</p>
                    </div>
                </li>
                <li>
                    <a class="uk-accordion-title" href="#">{% t faq.group_3.19.title %}</a>
                    <div class="uk-accordion-content">
                      <p class="list-angle font-compressed">{% t faq.group_3.19.description %}</p>
                    </div>
                </li>
                <li>
                    <a class="uk-accordion-title" href="#">{% t faq.group_3.20.title %}</a>
                    <div class="uk-accordion-content">
                      <p class="list-angle font-compressed">{% t faq.group_3.20.description %}</p>
                    </div>
                </li>
                <li>
                    <a class="uk-accordion-title" href="#">{% t faq.group_3.21.title %}</a>
                    <div class="uk-accordion-content">
                      <p class="list-angle font-compressed">{% t faq.group_3.21.description %}</p>
                    </div>
                </li>                                                        
            </ul>
          </div>
        </div>
      </div>
    </div>

<div class="uk-section section-color-2nd animated fadeIn">
  <div class="uk-container uk-width-3-4@m" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
      <div class="uk-width-1-1 uk-flex" uk-grid>
        <div class="uk-width-1-4@m">
          <h3 class="no-project-color">{% t faq.group_4.title %}</h3>
        </div>
        <div class="uk-width-3-4@m">
          <ul uk-accordion>
            <li>
                <a class="uk-accordion-title" href="#">{% t faq.group_4.22.title %}</a>
                <div class="uk-accordion-content">
                  <p class="list-angle font-compressed">{% t faq.group_4.22.description %}</p>
                </div>
            </li>                                                     
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="uk-section section-no-color animated fadeIn">
    <div class="uk-container uk-width-1-2@m uk-text-center " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
      <div class="uk-flex uk-flex-center" uk-grid>
        <div class="uk-width-1-1">
          <p class="font-lead">{% t faq.message_1 %}</p>
          <p class="font-lead font-color-1st-0" style="margin: -10px 0 10px;">{% t faq.message_2 %}</p>
          <a href="mailto:abrealcaldias@ciudadaniai.org" class="uk-button button-color-1st" uk-scroll>{% t faq.e-mail %}</a>
          <p class="font-lead font-color-1st-0" style="margin: 10px 0;">{% t faq.message_3 %}</p>
        </div>
      </div>
    </div>
  </div>
