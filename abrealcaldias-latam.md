---
layout: page-abrealcaldias-latam
sub-title: ¿Qué le falta a los municipios de Chile? La respuesta está en las personas
title: Abre Alcaldías en Latinoamérica
permalink: /abrealcaldias-latam/
welcome_desktop: ../assets/images/banners/header-abre-chile.png
welcome_mobile: ../assets/images/banners/header-abre-chile.png
---


<div class="bg-que-es que-es-abrechile">

<section class="uk-container  uk-container-small uk-margin" id="que-es">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">

            <p class="que-es-descripcion animated fadeInUp delay-1s fast uk-text-center">Desde 2020 promovemos el reconocimiento institucional de la participación ciudadana, los mecanismos de democracia directa ciudadana y la representación diversa a nivel local, nacional y regional en Latinoamérica y el Caribe.</p>

        </div>
    </div>
</section>
</div>



<section id="municipios-paises" class="latam-abre-mapa seccion-abrelatam">
    <div class="uk-container uk-container-medium row">
         <div class="content-map uk-margin-top">
             <h3 class="animated fadeInUp delay-1s fast title uk-text-bold uk-margin-remove-bottom">Contamos con experiencias en</h3>
        </div>

         <div class="mapa-abre-latam">
             <img src="/assets/images/abre/mapa-abre.png" alt="img mapa" class="img-mapa" width="100%">
         </div>
    </div>
</section>



<div class="bg-preguntas bg-preguntas-abrechile">
<section class="uk-container uk-container-small" id="preguntas">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <h3 class="animated fadeInUp delay-1s fast uk-text-center">¿En qué consiste la participación en Abre Alcaldías?</h3>
            <p class="descripcion-preguntas-frecuentes">La participación en el proyecto Abre Alcaldías consta de cuatro etapas</p>
        </div>

  <div class="uk-width-1">
        <ul uk-accordion="multiple: true">
            <li class="uk-open">
                <a class="uk-accordion-title" href="#">1. Etapa de formación virtual</a>
                <div class="uk-accordion-content">
                    <p>Contempla cinco módulos de clases que darán una mirada integral y holística al proceso de participación ciudadana en la toma de decisiones.</p>
                </div>
            </li>
            <li>
                <a class="uk-accordion-title" href="#">2. Diagnóstico y definición de plan de trabajo local</a>
                <div class="uk-accordion-content">
                    <p>A través de talleres de trabajo presenciales, se realizan análisis de contexto, diagnóstico y creación de plan de trabajo personalizado. Este espacio se realiza en colaboración con otras organizaciones de la sociedad civil para profundizar y enriquecer la instancia.</p>
                </div>
            </li>
            <li>
                <a class="uk-accordion-title" href="#">3. Etapa de implementación de planes de trabajo</a>
                <div class="uk-accordion-content">
                    <p>Los planes de trabajo creados en la etapa anterior son implementados por las y los funcionarios/as partícipes del proyecto. En esta instancia cuentan con el acompañamiento y la asesoría de las distintas organizaciones implementadoras y co implementadoras de la formación.</p>
                </div>
            </li>

            <li>
                <a class="uk-accordion-title" href="#">4. Etapa presencial de networking e intercambio</a>
                <div class="uk-accordion-content">
                    <p>El proyecto contempla, durante su realización, la creación de espacios de intercambio y creación de red, donde  gobiernos locales, organizaciones de la sociedad civil y ciudadanía se encuentren para promover un aprendizaje colectivo, intercambio e innovación en la región.</p>
                </div>
            </li>

        </ul>
   </div>

    </div>
</section>

</div>



<div class="contacto-correo">

<section class="uk-container uk-container-small uk-margin" id="que-es">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <p class="que-es-descripcion animated fadeInUp delay-1s fast uk-text-center">¿Tienes alguna pregunta?</p>
            <p class="que-es-descripcion-2 animated fadeInUp delay-1s fast uk-text-center">Contáctanos en abrealcaldias@ciudadaniai.org</p>
        </div>
    </div>
</section>
</div>

<div class="img-grupo">
    <img src="/assets/images/banners/bg-abre-chile.jpg" alt="img" class="img" width="100%">
</div>
