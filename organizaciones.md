---
layout: page
sub-title: Cómo trabajamos
title: Organizaciones
permalink: /organizaciones/
welcome_desktop: ../assets/images/abre/banner-herramientas.png
welcome_mobile: ../assets/images/abre/banner-movil-5.png
---

<section class="uk-container uk-margin-large" id="org">
    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
        <div class="uk-width-1">
            <h2 class="animated fadeInUp delay-1s fast">Organizaciones aliadas</h2>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m uk-margin-medium">
           <div class="animated fadeInUp delay-1s fast org">
                 <img src="/assets/images/abre/ollin-logo.png" alt="logo" class="logo-org">
                 <p>Trabaja por un México en donde impere el estado de derecho, a través de apoyar la consolidación de las instituciones democráticas y el acompañamiento personas capaces de transformar la realidad a través de la participación.</p>
                 <a href="https://ollinac.org/" target="_blank" class="enlace-org">https://ollinac.org/</a>
           </div>
        </div>


        <div class="uk-width-1@s uk-width-1-3@m uk-margin-medium">
           <div class="animated fadeInUp delay-1s fast org">
                 <img src="/assets/images/abre/extituto-logo.png" alt="logo" class="logo-org">
                 <p>Organización enfocada en el fortalecimiento de liderazgos y procesos colectivos, así como en el diseño de herramientas y tecnologías de innovación política y democrática.</p>
                 <a href="https://www.extituto.com/ " target="_blank" class="enlace-org">https://www.extituto.com/</a>
           </div>
        </div>


        <div class="uk-width-1@s uk-width-1-3@m uk-margin-medium">
           <div class="animated fadeInUp delay-1s fast org">
                 <img src="/assets/images/abre/red-ciudadana-logo.png" alt="logo" class="logo-org-red">
                 <p>Investiga sobre los obstáculos que impiden la participación ciudadana y crea herramientas innovadoras para el fortalecimiento institucional, Auditoria Social y fortalecimiento de espacios civicos.</p>
                 <a href="https://redciudadana.org/" target="_blank" class="enlace-org">https://redciudadana.org/</a>
           </div>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m uk-margin-medium">
           <div class="animated fadeInUp delay-1s fast org">
                 <img src="/assets/images/abre/accion-ciudadana-logo.png" alt="logo" class="logo-org-ac">
                 <p>Organización de la sociedad civil que se constituyó para fomentar una ciudadanía consciente y comprometida y para promover la transparencia en la gestión pública.</p>
                 <a href="https://accionciudadanagt.org/" target="_blank" class="enlace-org">https://accionciudadanagt.org/</a>
           </div>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m uk-margin-medium">
           <div class="animated fadeInUp delay-1s fast org">
                 <img src="/assets/images/abre/pares-logo.png" alt="logo" class="logo-org">
                 <p>Desde la investigación y el análisis crítico de la realidad nacional, busca contribuir a la defensa del Estado social de derecho y a la transformación de la democracia en Colombia.</p>
                 <a href="https://www.pares.com.co" target="_blank" class="enlace-org">https://www.pares.com.co</a>
           </div>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m uk-margin-medium">
           <div class="animated fadeInUp delay-1s fast org">
                 <img src="/assets/images/abre/escuela-mexicana-logo.png" alt="logo" class="logo-org">
                 <p>Organizacion para la formación de ciudadanos y servidores públicos a traves de una cultura de participación política responsable y reflexiva. Desarrolla núcleos de interacción con una actitud incluyente y transformadora para construir de comunidad</p>
                 <a href="http://eparticipa.org/" target="_blank" class="enlace-org">http://eparticipa.org/</a>
           </div>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m uk-margin-medium">
           <div class="animated fadeInUp delay-1s fast org">
                 <img src="/assets/images/abre/asuntosdelsur-logo.png" alt="logo" class="logo-org-as">
                 <p>Organización que diseña e implementa innovaciones políticas para desarrollar democracias paritarias, inclusivas y participativas.</p>
                 <a href="https://asuntosdelsur.org/" target="_blank" class="enlace-org">https://asuntosdelsur.org/</a>
           </div>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m uk-margin-medium">
           <div class="animated fadeInUp delay-1s fast org">
                 <img src="/assets/images/abre/techo-logo.png" alt="logo" class="logo-org">
                 <p>Desarrolla proyectos que mejoren las condiciones de hábitat y habitabilidad de las familias, en pos a una mejor calidad de vida y fortalecimiento de las capacidades.</p>
                 <a href="https://guatemala.techo.org/" target="_blank" class="enlace-org">https://guatemala.techo.org/</a>
           </div>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m uk-margin-medium">
           <div class="animated fadeInUp delay-1s fast org">
                 <img src="/assets/images/abre/no-ficcion-logo.png" alt="logo" class="logo-org">
                 <p>No-Ficción es un colectivo de periodistas interesados en la crónica narrativa, la investigación y la visualización de datos.</p>
                 <a href="https://www.no-ficcion.com/" target="_blank" class="enlace-org">https://www.no-ficcion.com/</a>
           </div>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m uk-margin-medium">
           <div class="animated fadeInUp delay-1s fast org">
                 <img src="/assets/images/abre/oppg-logo.png" alt="logo" class="logo-org">
                 <p>Busca contribuir al mejoramiento del diseño, implementación y seguimiento de las políticas públicas, así como promover espacios de formación democrática y participación ciudadana a nivel local y nacional.</p>
                 <a href="https://oppguayaquil.org/" target="_blank" class="enlace-org">https://oppguayaquil.org/</a>
           </div>
        </div>

        <div class="uk-width-1@s uk-width-1-3@m uk-margin-medium">
           <div class="animated fadeInUp delay-1s fast org">
                 <img src="/assets/images/abre/mexiro-logos.png" alt="logo" class="logo-org">
                 <p>Organización que busca erradicar la acumulación y centralización del poder que impiden el desarrollo sostenible.</p>
                 <a href="https://mexiro.org/" target="_blank" class="enlace-org">https://mexiro.org/</a>
           </div>
        </div>


    </div>
</section>    
