---
layout: page
sub-title: Cómo trabajamos
title: Proceso de formación
permalink: /proceso-de-formacion/
welcome_desktop: ../assets/images/abre/banner-herramientas.png
welcome_mobile: ../assets/images/abre/banner-movil-5.png
---


<section id="proceso-de-formacion" class="uk-container uk-container-small uk-margin-large">
  <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>
     <div class="uk-width-1 pasos">
       <div class="icono">
            <img src="/assets/images/abre/line-1.png" alt="icono" class="animated fadeInUp delay-1s fast">
       </div>
       <div class="texto">
            <h3 class="animated fadeInUp delay-1s fast">{% t proceso_de_formacion.paso_1 %}</h3>
            <p class="animated fadeInUp delay-1s fast">{% t proceso_de_formacion.descripcion_1 %}</p>
       </div>
     </div>
     <div class="uk-width-1 pasos">
       <div class="icono">
            <img src="/assets/images/abre/line-2.png" alt="icono" class="animated fadeInUp delay-1s fast">
       </div>
       <div class="texto">
            <h3 class="animated fadeInUp delay-1s fast">{% t proceso_de_formacion.paso_2 %}</h3>
            <p class="animated fadeInUp delay-1s fast">{% t proceso_de_formacion.descripcion_2 %}</p>
            <p class="animated fadeInUp delay-1s fast">{% t proceso_de_formacion.descripcion_list %}</p>
       </div>
     </div>
     <div class="uk-width-1 pasos">
       <div class="icono">
            <img src="/assets/images/abre/line-3.png" alt="icono" class="animated fadeInUp delay-1s fast">
       </div>
       <div class="texto">
            <h3 class="animated fadeInUp delay-1s fast">{% t proceso_de_formacion.paso_3 %}</h3>
            <p class="animated fadeInUp delay-1s fast">{% t proceso_de_formacion.descripcion_3 %}</p>
       </div>
     </div>
     <div class="uk-width-1 pasos">
       <div class="icono">
            <img src="/assets/images/abre/line-4.png" alt="icono" class="animated fadeInUp delay-1s fast">
       </div>
       <div class="texto">
            <h3 class="animated fadeInUp delay-1s fast">{% t proceso_de_formacion.paso_4 %}</h3>
            <p class="animated fadeInUp delay-1s fast">{% t proceso_de_formacion.descripcion_4 %}</p>
       </div>
     </div>
     <div class="uk-width-1 pasos">
       <div class="icono">
            <img src="/assets/images/abre/line-5.png" alt="icono" class="animated fadeInUp delay-1s fast">
       </div>
       <div class="texto">
            <h3 class="animated fadeInUp delay-1s fast">{% t proceso_de_formacion.paso_5 %}</h3>
            <p class="animated fadeInUp delay-1s fast">{% t proceso_de_formacion.descripcion_5 %}</p>
       </div>
     </div>

     <div class="uk-width-1 btn uk-margin-large">
        <a href="{{ site.baseurl_root }}/nuestras-herramientas" class="animated fadeInUp delay-1s fast btn-abre">Ir a Herramientas ></a>
     </div>

  </div>
</section>
